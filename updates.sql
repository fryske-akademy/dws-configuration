CREATE VIEW complex AS
select lemma from artikel
where substr(onderdeelstata,13,1)='2' or substr(onderdeelstata,13,1)='3';

CREATE VIEW lemma_5000 AS
select lemma from artikel
where (redacteur=66 or redacteur=76)
and artikelfase=1
and substr(onderdeelstata,13,1)='3';

update artikel set onderdeelstata=concat(substr(onderdeelstata,1,15),'3') where lemma='';

update artikelonderdeel set naam='Leescommentaar', abbr='LC' where id=3;

update artikel set onderdeelstata=concat(substr(onderdeelstata,1,2),'2',substr(onderdeelstata,4,13)) where
tekst like '%Leescommentaar%' and onderdeelstata!='';

update artikel set onderdeelstata='0020000000000000' where
    tekst like '%Leescommentaar%' and onderdeelstata='';

CREATE VIEW artikelxml AS select
    a.id as id,
    d.id as pid,
    a.lemma AS lemma,
    a.tekst AS tekst,
    a.artikelfase AS artikelfase,
    b.username AS username,
    a.laatste_wijziging AS laatste_wijziging,
    a.artikelfasedatum AS artikelfasedatum,
    c.src_article as linked
    from artikel a
    join user b on a.redacteur = b.id
    join pid d on d.article=a.id
    left outer join link c on c.dst_pid=d.id and c.src_type=3
    where a.redacteur > 61
    group by a.lemma, linked;
