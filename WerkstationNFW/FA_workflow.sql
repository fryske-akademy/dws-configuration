delete from artikelonderdeeldatum;
delete from artikelonderdeel;
delete from heeft_gewerkt_aan where `user` < 62;
update artikel set onderdeelstata='', gelockt_door=0,status=0,artikelfase=0,artikelfasedatum=null;
delete from `user` where id < 62;
INSERT INTO `artikelonderdeel` VALUES (1,'Oersetting',0,'OS'),(2,'Stavering',0,'ST'),(3,'Ofbrekking',0,'OB'),
(4,'Fleksje',0,'FL'),(5,'Utspraak',0,'UT'),(6,'Wurdsoarte',0,'WS'),(7,'bijzonderheden gebruik',1,'BG'),(8,'definitie',1,'DEF'),
(9,'semagram',1,'SEM'),(10,'Kombinaasje per type',0,'KT'),(11,'Ferbining',0,'FERB'),(12,'Sprekwurd',0,'SW'),(13,'foarbylden',0,'FB'),
(14,'Wurdfamylje',0,'WF'),(15,'Data korpora ynbrocht',0,'KORP'),(16,'Alle skiften',0,'SK');
update artikelfase set naam_in_lijst='ûnbewurke' WHERE naam='onbewerkt';
update artikelfase set naam_in_lijst='yn bewurking' WHERE naam='in bewerking';
update artikelfase set naam_in_lijst='kollegiaal lêze' WHERE naam='neo-lijst';
update artikelfase set naam_in_lijst='einredaksje' WHERE naam='naar hoofdredaktie' or naam='naar hoofdredactie';
update artikelfase set naam_in_lijst='giet online' WHERE naam='gaat online';
insert into artikelfase (id, naam, naam_in_lijst) values (13,'net opnommen','net opnommen');
delete from artikelfase where naam not in ('net opnommen','onbewerkt','in bewerking','neo-lijst','naar hoofdredactie','naar hoofdredaktie','gaat online','online');
