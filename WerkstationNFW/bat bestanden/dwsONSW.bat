@echo off
if not DEFINED IS_MINIMIZED set IS_MINIMIZED=1 && start "" /MIN "dwsONSW.bat" && exit
echo dit window sluit vanzelf als dws (nfw/onsw) sluit
pushd \\fafs02\groups
set DWS_CONFIG_PATH=DWSX\DwsONSW\WerkstationNFW\config
start /WAIT /MIN java -Dfile.encoding=UTF-8 -jar DWSX\DwsWerkstation-fa-2.16-jar-with-dependencies.jar 1>> "DWSX\DwsONSW\dws%username%.log" 2>>&1
exit
