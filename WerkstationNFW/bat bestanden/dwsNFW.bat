if exist c:\dwsx\ (
    pushd c:\dwsx
    if exist dwsx\ (
        echo "ER MOET ALLEEN EEN C:\DWSX MAP ZIJN! GEEN GENESTE C:\DWSX\DWSX MAP! "
        pause
        exit 1
    )
    if exist WerkstationNFW\dwsNFW.bat (
        echo "dwsNFW.bat NIET GEVONDEN, IS DE ZIP GOED UITGEPAKT?"
        pause
        exit 1
    )
    set DWS_CONFIG_PATH=DwsNFW\WerkstationNFW\config
    java -Dfile.encoding=UTF-8 -jar DwsWerkstation-1.5-SNAPSHOT-jar-with-dependencies.jar
)