<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    exclude-result-prefixes="xs xsi fa"
    xmlns:fa="http://www.fryske-akademy.nl/functions/pietermasereeuw"
    version="2.0">
    
    <xsl:output method="xml" encoding="UTF-8" indent="no"/>
    
    <xsl:template match="earste_en_tredde_persoan_dt."/>
    <xsl:template match="twadde_persoan_dt."/>
    
    <xsl:template match="NFWTiidwurdFoarmen"/>
    
    <xsl:template match="Meervoud_o.t.t.[not(following-sibling::Enkelvoud_o.v.t)]">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
        <xsl:copy-of select="../earste_en_tredde_persoan_dt."/>
        <xsl:copy-of select="../twadde_persoan_dt."/>
    </xsl:template>
    
    <xsl:template match="Enkelvoud_o.v.t">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
        <xsl:copy-of select="../earste_en_tredde_persoan_dt."/>
        <xsl:copy-of select="../twadde_persoan_dt."/>
    </xsl:template>
    
    <xsl:template match="NFWRealisaasjePlusOersettingen/Realisatie">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
        <xsl:if test="not(../NFWOersettingen)">
            <NFWOersettingen>
                <NFWOersetting>
                    <NFWFoarm>---</NFWFoarm>
                </NFWOersetting>
            </NFWOersettingen>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="NFWTekstPlusOersettingen/Tekst">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
        <xsl:if test="not(../NFWOersettingen)">
            <NFWOersettingen>
                <NFWOersetting>
                    <NFWFoarm>---</NFWFoarm>
                </NFWOersetting>
            </NFWOersettingen>
        </xsl:if>
    </xsl:template>
    
    <!-- Default template: copy everything and keep matching. -->
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>