<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/loose.dtd" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" />

	<!-- Hoofdtemplate -->
	<xsl:template match="/artikel">
		<html>
			<head>
				<title>Artikeloverzicht</title>
				<link rel='stylesheet' type='text/css' href='anw.css' />
			</head>
			<body>
				<h1>Artikel '<xsl:value-of select="Lemma/Lemmavorm" />'</h1>
				<xsl:call-template name='processChildren' />
			</body>
		</html>
	</xsl:template>

	<!-- Standaard "process alle kinderen van deze node" template (wordt op meerdere plaatsen 'aangeroepen') -->
	<xsl:template name='processChildren'>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<!-- Betekenisnummer tonen we niet als node (die wordt nl. al bij de node Kernbetekenis of Subbetekenis getoond) -->
	<xsl:template match="inflate|Betekenisnummer">
	</xsl:template>

	<!-- First-level nodes tonen we met h2-kop (behalve BetekenisEnGebruik) -->
	<xsl:template match="artikel/*[name() != 'BetekenisEnGebruik']">
		<h2><xsl:value-of select="name()" /></h2>
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Kernbetekenis en Subbetekenis tonen we met h2 kop, en we zoeken het Betekenisnummer erbij -->
	<xsl:template match="Kernbetekenis|Subbetekenis">
		<h2><xsl:value-of select="name()" /><xsl:text> </xsl:text><xsl:value-of select="betekenisInfo/Betekenisnummer" /></h2>
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- De volgende *Body elementen gaan (hopelijk) verdwijnen;
		We tonen hiervoor in ieder geval geen kop -->
	<xsl:template match="Semagram/*|betekenisInfo|combinatiesBody|voorbeeldBody|ContextantenBody|spreekwoordenBody|verbindingenBody">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Voor de volgende *Body elementen geen kop tonen -->
	<xsl:template match="AfbrekingBody|meervoudBody|domeinBody|AntoniemBody|RealisatieBody|definitieBody|genusBody|SpellingvariantBody|VormvariantBody|VoorzetselBody|AardBody">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Niet helemaal zeker of we hiervoor een kop moeten tonen; voorlopig maar weglaten -->
	<xsl:template match="SubstantiefCombi|CombinatieInfo|Info|BetekenisEnGebruik">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Pragmatiek -->
	<xsl:template match="Pragmatiek">
		<div class='pragmatiek'>
			<b><xsl:value-of select="name()" /></b><br/>
			<xsl:value-of select="text()" />
			<div class='pragmatiek_inner'>
				<xsl:call-template name='processChildren' />
			</div>
		</div>
	</xsl:template>
	
	<!-- De standaard Node template (als node niet gematcht is door een van de bovenstaande templates, wordt-ie zo getoond) -->
	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="name() != 'Contextanten' and not(*)">
				<!-- Node zonder kinderen (en niet Contextanten) -->
				<xsl:choose>
					<xsl:when test="string-length(name()) &lt;= 15">
						<!-- Korte elementnaam -->
						<table class='prop'>
							<tr>
								<th><xsl:value-of select="name()" />:</th>
								<td><xsl:value-of select="text()" /></td>
							</tr>
						</table>
					</xsl:when>
					<xsl:otherwise>
						<!-- Lange elementnaam -->
						<table class='prop'>
							<tr>
								<th class='breed'><xsl:value-of select="name()" />:</th>
								<td><xsl:value-of select="text()" /></td>
							</tr>
						</table>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Node met kinderen (of Contextanten) -->
				<h3><xsl:value-of select="name()" /></h3>
				<table class='prop'><tr><td>
					<xsl:value-of select="text()" />
					<xsl:call-template name='processChildren' />
				</td></tr></table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>

