<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:output encoding="utf-8" method="text" />
    
    <xsl:template match="q[starts-with(text(),'– ') and not(text()='– ')
    and note[@lang='nl'] and not(starts-with(note/text(),'nl.') or starts-with(note/text(),', nl.')) and
    not(note/text()='in' or note/text()='in:' or note/text()='Id.') and
    not(starts-with(note/text(),'(') and (ends-with(note/text(),')') or ends-with(note/text(),'). ')))]">
        <xsl:choose>
            <xsl:when test="ends-with(text(),', ')">
                <xsl:variable name="l" select="string-length(substring-after(text(),'– '))"/>
                <xsl:value-of select="note[@lang='nl']/text()"/>|<xsl:value-of select="substring(substring-after(text(),'– '),1,$l - 2)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="note[@lang='nl']/text()"/>|<xsl:value-of select="substring-after(text(),'– ')"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>
</xsl:text>
    </xsl:template>
    

    <xsl:template match="node()|@*" priority="-1">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
</xsl:stylesheet>
