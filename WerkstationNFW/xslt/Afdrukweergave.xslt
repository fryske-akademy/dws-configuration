<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>

    <xsl:template match="@*|node()" priority="-3">
        <xsl:if test="not(name()='')">
            <xsl:call-template name="toDo"/>
        </xsl:if>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>
    
    <xsl:template match="SpellingEnFlexie">
        <xsl:call-template name="toDo"/>
        <xsl:apply-templates select="Varianten"/>
    </xsl:template>

    <xsl:template match="artikel">
        <html>
            <head>
                <style>
                    .toDo {
                        display: none
                    }
                    * {
                        box-sizing: border-box;
                    }
                    .container .container, .container .keyValue {
                        margin-left: 10px
                    }
                    .Oersettingen, .container .keyValue, .container.Oersetting, .Oersetting .Woordsoort, .Woordsoort *:not(.toDo) {
                        display: inline-block
                    }
                    .Combinaties, .Verbindingen, .definitieBody, .Leescommentaar {
                        margin-bottom: 10px;
                    }
                    .definitieBody, .Leescommentaar {
                        margin-top: 10px;
                    }
                    .container.GeslachtBody {
                        margin-left: 0px;
                    }
                    .keyValue.OersetTaljochting, .keyValue.Woordsoort, .Leescommentaar, .LeesCommentaar span {
                        display: block
                    }
                    .container.Voorbeeld, .container.Woordsoort, .container.BijzonderhedenGebruik {
                        margin: 2px
                    }
                    .container.RealisaasjePlusOersettingen, .container.Verbindingen {
                        margin: 2px
                    }
                    .keyValue span {
                        font-weight: bold
                    }
                    .Varianten .Opmerkingen {
                        margin-left: 20px;
                    }
                    h4 {
                        margin: 10px
                    }
                </style>
                <script type="text/javascript">
                    var css = document.styleSheets[0];
                    var show = false;
                    function toggleToDo() {
                        if (show) {
                            css.deleteRule(0);
                            css.insertRule(".toDo {display:none}",0);
                        } else {
                            css.deleteRule(0);
                            css.insertRule(".toDo {display:;color:gray}",0);
                        }
                        show = !show;
                    }
                </script>
            </head>
            <body>
                <input type="button" value="toon/verberg weggelaten info" onclick="toggleToDo();"/><br/>
                <xsl:apply-templates select="node()"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Minidefinitie">
        <xsl:call-template name="keyValue">
            <xsl:with-param name="key" select="concat(count(preceding::Minidefinitie) + 1,' ',name())"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="Voorzetsel|Realisatie|TrefwoordType|Koepelnummer|Lemmavorm[not(string(.)=string(preceding::Lemmavorm))]|Definitieaanvulling|Definitie|OersetTaljochting|Taljochting|Foarm[.!='---']|Gebrûk|Vorm|Vormvariant|ArtikelRef|Tekst|Commentaar|Betekenisletter[count(ancestor::Verbindingen/Info) > 1]|Betekenisnummer[count(ancestor::Spreekwoorden/Info) > 1]|Leescommentaar|Opmerkingen|Woordvorm">
        <xsl:call-template name="keyValue"/>
    </xsl:template>

    <xsl:template match="Koepelartikel|BijzonderhedenGebruik[*]|BetekenisEnGebruik[*]|Subbetekenis|Varianten">
        <xsl:call-template name="container"/>
    </xsl:template>

    <xsl:template match="voorbeeldBody">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="'Voorbeelden'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="Spreekwoorden">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="concat('&#x25AC; ',name())"/>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="verbindingenBody">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="'&#x25CF; Verbindingen'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="combinatiesBody">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="'&#x25B6; Combinaties'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="*[contains(name(),'Rol') and contains(name(..),'Combi')]" priority="1">
        <xsl:call-template name="keyValue"/>
    </xsl:template>
    
    <xsl:template match="RealisaasjePlusOersettingen|Combinaties|Verbindingen|Oersetting[Foarm[.!='---']]|Voorbeeld|definitieBody|CombinatieInfo|RealisatieBody|VoorzetselBody|TekstPlusOersettingen|Oersettingen|*[name(../../..)='combinatiesBody']|Woordsoort/*|BijzonderhedenGebruik/domeinBody|BijzonderhedenGebruik/Frequentie|LegaleVariant">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="''"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="Voorbeeld/Bron|Voorbeeld/BronID|Voorbeeld/Auteur|Voorbeeld/URL|Voorbeeld/Datering">
        <xsl:call-template name="toDo"/>
    </xsl:template>
    <xsl:template match="Voorbeeld/*" priority="-2">
        <xsl:call-template name="keyValue"/>
    </xsl:template>

    <xsl:template match="BetekenisEnGebruik/Kernbetekenis/betekenisInfo/Woordsoort">
        <xsl:call-template name="container">
            <xsl:with-param name="name">
                <xsl:if test="count(ancestor::BetekenisEnGebruik/Kernbetekenis) > 1">
                    <xsl:value-of select="count(ancestor::BetekenisEnGebruik/Kernbetekenis/preceding-sibling::Kernbetekenis) + 1"/>
                </xsl:if>
                <xsl:value-of select="concat(' Woordsoort: ',Type)"/>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="Woordsoort">
        <xsl:call-template name="container">
            <xsl:with-param name="name" select="concat('Woordsoort: ',Type)"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="Lidwoord">
        <xsl:call-template name="keyValue">
            <xsl:with-param name="val" select="AardLidwoord|Soort"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="Woordsoort/*/*">
        <xsl:call-template name="keyValue"/>
        <xsl:apply-templates select="node()"/>
    </xsl:template>

    <xsl:template match="BijzonderhedenGebruik/domeinBody/*|BijzonderhedenGebruik/Frequentie/*">
        <xsl:call-template name="keyValue"/>
    </xsl:template>

    <xsl:template match="BijzonderhedenGebruik/*" priority="-2">
        <xsl:call-template name="keyValue"/>
    </xsl:template>

    <xsl:template name="keyValue">
        <xsl:param name="key" select="name()"/>
        <xsl:param name="val" select="."/>
        <span class="keyValue {name()}">
            <span><xsl:value-of select="$key"/>: </span> 
            <xsl:value-of select="$val"/>
        </span>
    </xsl:template>

    <xsl:template name="toDo">
        <div class="toDo" title="weggelaten info">
            <xsl:value-of select="name()"/>
        </div>
    </xsl:template>

    <xsl:template name="container">
        <xsl:param name="name" select="name()"/>
        <div class="container {name()}">
            <xsl:if test="$name!=''">
                <h4>
                    <xsl:value-of select="$name"/>
                </h4>
            </xsl:if>
            <xsl:apply-templates select="node()"/>
        </div>
    </xsl:template>

</xsl:stylesheet>

