<?xml version="1.0" encoding="UTF-8"?>

<!--
Pieter Masereeuw, 23 september 2016.
Dit stylesheet verzorgt de conversie van ANW-materiala naar NFW-materiaal. Het is een stylesheet uit
de tweede fase van het project, waarbij het uitgangspunt is dat een dump van de ANW-data eenmalig wordt
omgezet om vervolgens een eigen leven te gaan leiden, zonder dat ANW-aanpassingen nog (automatisch)
ingevlochten moeten worden.
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    exclude-result-prefixes="xs xsi fa"
    xmlns:fa="http://www.fryske-akademy.nl/functions/pietermasereeuw"
    version="2.0">
    
    <xsl:output method="xml" encoding="UTF-8" indent="no"/>
    
    <!-- Default template: copy everything and keep matching. -->
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*" mode="#current"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Remove the ANW Schema reference -->
    <xsl:template match="@xsi:*"/>
    
    <!--
    <xsl:template match="/*">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xsi:noNamespaceSchemaLocation">file:/home/pieter/fryske-akademy/software/DWS-src/INL-DWS/DwsFiles/install/NFWSchema/NFWSchema-main.xsd</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    -->
            
    <xsl:template name="doWurdfamyljelid">
        <xsl:param name="text" as="xs:string" required="yes"/>
        <Wurdfamyljelid>
            <Boarnetaal><xsl:value-of select="$text"/></Boarnetaal>
            <Doeltaal>---</Doeltaal>
        </Wurdfamyljelid>
    </xsl:template>
    
    <xsl:template name="doWurdfamyljeChildElement">
        <xsl:for-each select="tokenize(., '\s*;\s*')">
            <xsl:analyze-string select="." regex="(.*)\[([^\]]*)\](.*)">
                <xsl:matching-substring>
                    <xsl:variable name="start" select="regex-group(1)"/>
                    <xsl:variable name="middle" select="regex-group(2)"/>
                    <xsl:variable name="end" select="regex-group(3)"/>
                    <xsl:for-each select="tokenize($middle, '\s*,\s*')">
                        <xsl:call-template name="doWurdfamyljelid">
                            <xsl:with-param name="text" select="concat($start, ., $end)"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:call-template name="doWurdfamyljelid">
                        <xsl:with-param name="text" select="."/>
                    </xsl:call-template>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="Woordfamilie">
      <Wurdfamylje>
          <xsl:apply-templates select="@* | node()"/>
      </Wurdfamylje>        
    </xsl:template>
    
    <xsl:template match="Woordfamilie/Afleidingen">
        <Ofliedingen>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </Ofliedingen>
    </xsl:template>
    
    <xsl:template match="Woordfamilie/SamenstellingenMetTrefwoordAlsRechterlid">
        <GearstallingenMeiStekwurdAsRjochterlid>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </GearstallingenMeiStekwurdAsRjochterlid>
    </xsl:template>
    
    <xsl:template match="Woordfamilie/SamenstellingenMetTrefwoordAlsLinkerlid">
        <GearstallingenMeiStekwurdAsLofterlid>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </GearstallingenMeiStekwurdAsLofterlid>
    </xsl:template>
    
    <xsl:template match="Woordfamilie/SamenstellendeAfleidingenMetHetTrefwoord">
        <GearstallendeOfliedingenMeiItStekwurd>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </GearstallendeOfliedingenMeiItStekwurd>
    </xsl:template>
    
    <xsl:template match="Woordfamilie/SamenstellendeSamenstellingenMetHetTrefwoord">
        <GearstallendeGearstallingenMeiItStekwurd>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </GearstallendeGearstallingenMeiItStekwurd>
    </xsl:template>
    
    <xsl:template match="Woordfamilie/OverigeWoordFamilieLeden">
        <OareWurdFamyljeLeden>
            <xsl:call-template name="doWurdfamyljeChildElement"/>
        </OareWurdFamyljeLeden>
    </xsl:template>
    
    <xsl:template name="genOersettingen">
        <Oersettingen>
            <Oersetting>
                <Foarm>---</Foarm>
            </Oersetting>
        </Oersettingen>
    </xsl:template>
    
    <xsl:template match="*[self::Kernbetekenis or self::Subbetekenis]/betekenisInfo">
        <!-- This template follows the ANW schema for betekenisInfo in order to find the correct place to insert te Oersettingen element. -->
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="Betekenisnummer|Lemma|Woordsoort|SpellingEnFlexie|Uitspraak|Woordvorming|BijzonderhedenGebruik|definitieBody"/>
            <xsl:call-template name="genOersettingen"/>
            <xsl:apply-templates select="Woordrelaties|Varianten|Betekenisbetrekking|Semagram|voorbeeldBody|combinatiesBody|verbindingenBody|spreekwoordenBody|Woordfamilie|Media|Etymologie|Commentaar"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template name="doRealisaasjePlusOersettingen">
        <xsl:param name="text" as="xs:string" required="yes"/>
        <RealisaasjePlusOersettingen>
            <Realisatie><xsl:value-of select="$text"/></Realisatie>
            <xsl:call-template name="genOersettingen"/>
        </RealisaasjePlusOersettingen>
    </xsl:template>
    
    <xsl:template match="Realisatie">
        <xsl:for-each select="tokenize(., '\s*;\s*')">
            <xsl:analyze-string select="." regex="(.*)\[([^\]]*)\](.*)">
                <xsl:matching-substring>
                    <xsl:variable name="start" select="regex-group(1)"/>
                    <xsl:variable name="middle" select="regex-group(2)"/>
                    <xsl:variable name="end" select="regex-group(3)"/>
                    <xsl:for-each select="tokenize($middle, '\s*,\s*')">
                        <xsl:call-template name="doRealisaasjePlusOersettingen">
                            <xsl:with-param name="text" select="concat($start, ., $end)"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:call-template name="doRealisaasjePlusOersettingen">
                        <xsl:with-param name="text" select="."/>
                    </xsl:call-template>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="Voorbeeld/Tekst">
        <!-- TODO Tekst is optioneel in het ANW-schema; wat te doen als het ontbreekt? -->
        <TekstPlusOersettingen>
            <xsl:copy>
                <xsl:apply-templates select="node() | @*"/>
            </xsl:copy>
            <xsl:call-template name="genOersettingen"/>
        </TekstPlusOersettingen>
    </xsl:template>
    
    <xsl:template match="Info/definitieBody">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
        <xsl:if test="not(following-sibling::definitieBody)"><xsl:call-template name="genOersettingen"/></xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
