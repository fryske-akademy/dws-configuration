<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:output method="xml" encoding="UTF-8" indent="no"/>
    
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="artikel/Leescommentaar">
                <xsl:message><xsl:value-of select="artikel/Lemma/Lemmavorm"/>: Leescommentaar moved</xsl:message>
                <artikel>
                    <xsl:copy-of select="artikel/@*"/>
                    <xsl:copy-of select="artikel/node()[name()='Leescommentaar']"/>
                    <xsl:copy-of select="artikel/node()[name()!='Leescommentaar']"/>
                </artikel>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="artikel"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>
