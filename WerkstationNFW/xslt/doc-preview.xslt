<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/loose.dtd" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" />

	<!-- Hoofdtemplate -->
	<xsl:template match="/artikel">
		<html>
			<head>
				<title>Artikeloverzicht</title>
				<link rel='stylesheet' type='text/css' href='adr.css' />
			</head>
			<body style="margin: 10px; font-size: 18pt;">
				<h1>Artikel '<xsl:value-of select="Lemma/Lemmavorm" />'</h1>
				<xsl:call-template name='processChildren' />
			</body>
		</html>
	</xsl:template>

	<!-- Standaard "process alle kinderen van deze node" template (wordt op meerdere plaatsen 'aangeroepen') -->
	<xsl:template name='processChildren'>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
		</xsl:for-each>
	</xsl:template>

	<!-- Betekenisnummer tonen we niet als node (die wordt nl. al bij de node Kernbetekenis of Subbetekenis getoond) -->
	<xsl:template match="inflate|Betekenisnummer">
	</xsl:template>

	<!-- First-level nodes tonen we met h2-kop (behalve BetekenisEnGebruik) -->
	<xsl:template match="artikel/*[name() != 'BetekenisEnGebruik']">
		<h2><u><xsl:value-of select="name()" /></u></h2>
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Kernbetekenis en Subbetekenis tonen we met h2 kop, en we zoeken het Betekenisnummer erbij -->
	<xsl:template match="Kernbetekenis|Subbetekenis">
		<h2><u><xsl:value-of select="name()" /><xsl:text> </xsl:text><xsl:value-of select="betekenisInfo/Betekenisnummer" /></u></h2>
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Link: display niet-autoritatieve elementen lemma en desc -->
	<xsl:template match="link">
		<xsl:value-of select='lemma' /><xsl:text> </xsl:text><xsl:value-of select='desc' /><br/>
	</xsl:template>

	<!-- De volgende *Body elementen gaan (hopelijk) verdwijnen;
		We tonen hiervoor in ieder geval geen kop -->
	<xsl:template match="Semagram/*[name() != 'Semagramaanhef']|betekenisInfo|combinatiesBody|voorbeeldBody|ContextantenBody|spreekwoordenBody|verbindingenBody">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Voor de volgende *Body elementen geen kop tonen -->
	<xsl:template match="AfbrekingBody|meervoudBody|domeinBody|AntoniemBody|RealisatieBody|definitieBody|genusBody|SpellingvariantBody|VormvariantBody|VoorzetselBody|AardBody">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Niet helemaal zeker of we hiervoor een kop moeten tonen; voorlopig maar weglaten -->
	<xsl:template match="SubstantiefCombi|CombinatieInfo|Info|BetekenisEnGebruik">
		<xsl:call-template name='processChildren' />
	</xsl:template>

	<!-- Pragmatiek tonen we in een tabel
	<xsl:template match="Pragmatiek">
		<table cellpadding='0' cellspacing='0'><tr><td width='30'></td><td>
			<table border='1'><tr><td>
			<b><xsl:value-of select="name()" /></b><br/>
			<xsl:value-of select="text()" />
			<xsl:call-template name='processChildren' />
			</td></tr></table>
		</td></tr></table>
	</xsl:template> -->
	
	<!-- De standaard Node template (als node niet gematcht is door een van de bovenstaande templates, wordt-ie zo getoond) -->
	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="name() != 'Contextanten' and not(*)">
				<!-- Node zonder kinderen (en niet Contextanten) -->
				<xsl:choose>
					<xsl:when test="string-length(name()) &lt;= 15">
						<!-- Korte elementnaam; op dezelfde regel plaatsen -->
						<table>
							<tr>
								<td valign='top' width='130'><b><xsl:value-of select="name()" />:</b></td>
								<td valign='top'><xsl:value-of select="text()" /></td>
							</tr>
						</table>
					</xsl:when>
					<xsl:otherwise>
						<!-- Lange elementnaam; op aparte regel plaatsen -->
						<table>
							<tr>
								<td valign='top' colspan='2'><b><xsl:value-of select="name()" />:</b></td>
							</tr>
							<tr>
								<td valign='top' width='130'></td>
								<td valign='top'><xsl:value-of select="text()" /></td>
							</tr>
						</table>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Node met kinderen (of Contextanten) -->
				<br/><font size='+1'><b><xsl:value-of select="name()" /></b></font><br/>
				<table cellpadding='0' cellspacing='0'><tr><td width='30'></td><td>
					<xsl:value-of select="text()" />
					<xsl:call-template name='processChildren' />
				</td></tr></table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


</xsl:stylesheet>

