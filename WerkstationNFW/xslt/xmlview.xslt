<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/loose.dtd" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" />

	<xsl:template match="/">
		<html>
			<style>
				body
				{
					font-family: sans-serif;
					font-size: 12;
				}

				div.indent
				{
					margin-left: 10px;
				}

				span.punct
				{
					color: #0000ff;
				}

				span.tagname
				{
					font-weight: normal;
					font-style: normal;
					color: #800000;
				}

				span.commenttext
				{
					color: #808080;
				}

				span.text
				{
					font-weight: bold;
				}

				span.att
				{
					color: #f00;
				}
			</style>
			<body style='font-size: 16pt;'>
				<span class='punct'><xsl:text>&lt;?xml version="1.0" encoding="utf-8" ?&gt;</xsl:text></span><br/>
				<xsl:for-each select="*|comment()|processing-instruction()|text()">
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="comment()">
		<b><xsl:text>&lt;!--</xsl:text></b>
		<span class='commenttext'><xsl:value-of select="." /></span>
		<b><xsl:text>--&gt;</xsl:text></b><br/>
	</xsl:template>

	<xsl:template match="processing-instruction()">
		<span class='punct'>
			<xsl:text>&lt;?</xsl:text>
			<xsl:value-of select="name()" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="." />
			<xsl:text>?&gt;</xsl:text>
		</span><br/>
	</xsl:template>

	<xsl:template match="text()">
		<span class='text'>
			<xsl:value-of select="." />
		</span>
	</xsl:template>

	<xsl:template match="@*">
		<xsl:text> </xsl:text>
		<span class='att'>
			<xsl:value-of select="name()" />
		</span>
		<xsl:text>=&quot;</xsl:text>
		<span class='text'>
			<xsl:value-of select="." />
		</span>
		<xsl:text>&quot;</xsl:text>
	</xsl:template>

	<xsl:template match="namespace">
		<xsl:text> </xsl:text>
		<span class='att'>
			<xsl:value-of select="name()" />
		</span>
		<xsl:text>=&quot;</xsl:text>
		<span class='text'>
			<xsl:value-of select="." />
		</span>
		<xsl:text>&quot;</xsl:text>
	</xsl:template>

	<xsl:template match="*">
		<span class='punct'><xsl:text>&lt;</xsl:text></span>
		<span class='tagname'><xsl:value-of select="name()" /></span>
		<xsl:if test="@*">
			<xsl:for-each select="@*">
				<xsl:apply-templates select="." />
			</xsl:for-each>
		</xsl:if>
		<span class='punct'><xsl:text>&gt;</xsl:text></span>
		<xsl:choose>
			<xsl:when test="*">
				<div class='indent'>
				<xsl:for-each select="*">
					<xsl:apply-templates select="." />
				</xsl:for-each>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="*|comment()|processing-instruction()|text()">
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<span class='punct'><xsl:text>&lt;/</xsl:text></span>
		<span class='tagname'><xsl:value-of select="name()" /></span>
		<span class='punct'><xsl:text>&gt;</xsl:text></span><br/>
	</xsl:template>


</xsl:stylesheet>

