package org.fryske_akademy.dws.publish;

import com.vectorprint.configuration.cdi.Property;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.MimeMessage;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.validation.FA_RngValidationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Named
@ApplicationScoped
public class PublishBeanImpl implements PublishBean {

    public static final Logger LOGGER = LoggerFactory.getLogger(PublishBeanImpl.class);

    public static final int CANDIDATE = 1;

    @Inject
    @Property
    private String smtp;
    @Inject
    @Property
    private String adminmail;
    @Inject
    @Property
    private String from;

    @Inject
    @Property(defaultValue = {"1", "2", "8", "12", "13"})
    private int[] statussen;

    @Inject
    @Property(defaultValue = "true")
    private boolean mailConversion;

    @Inject
    @Property(defaultValue = "true")
    private boolean logLemma;

    @Inject
    private CrudReadService crudReadService;

    private String currentPubDate;

    @Inject
    @Property(defaultValue = "true")
    private String voorredactie;

    private final File xsltFile = new File(APP_DIR + DWS_TO_TEI);

    private final StringBuilder publog = new StringBuilder();
    private boolean converted;

    protected String now() {
        return LocalDateTime.now().toString();
    }

    @Override
    public void init() {
        currentPubDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATEFORMAT));
        try {

            if (!ONFW_DIR.exists()) Files.createDirectory(ONFW_DIR.toPath());
            else Files.walk(ONFW_DIR.toPath()).filter(p -> p.toFile().isFile()).forEach(p -> {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });

            if (!DATA_DIR.exists()) Files.createDirectory(DATA_DIR.toPath());
            else Files.walk(DATA_DIR.toPath()).filter(p -> p.toFile().isFile()).forEach(p -> {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            });
            publog.setLength(0);
            publog.append("start converting" +
                    ": " + now() + "\n");
            converted = false;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void mail(String mesg, String body) {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtp);

        try {
            Session session = Session.getInstance(props);
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, adminmail);
            msg.setSubject(mesg);
            msg.setSentDate(new Date());
            msg.setText(body);
            Transport.send(msg);
        } catch (Exception mex) {
            LOGGER.error("unable to mail " + body, mex);
        }
    }

    @Override
    public List<ArtikelXML> copyFromDws() throws IOException {
        List<ArtikelXML> toProcess = new ArrayList<>(50) {
            private Set<String> lemmas = new HashSet<>(50);
            @Override
            public boolean add(ArtikelXML artikelXML) {
                if (!lemmas.contains(artikelXML.getLemma())) {
                    super.add(artikelXML);
                    return lemmas.add(artikelXML.getLemma());
                }
                return false;
            }
        };
        System.out.println(now() + " loading to " + ONFW_DIR);
        List<ArtikelXML> artikelen = crudReadService.findAll(ArtikelXML.class);
        System.out.println("loaded %d artikelen".formatted(artikelen.size()));
        Calendar cal = Calendar.getInstance();
        for (ArtikelXML artikelXML : artikelen) {
            if (artikelXML.getXml() == null) {
                LOGGER.warn("xml null for " + artikelXML.getLemma());
                continue;
            }
            LocalDateTime previousConversion = null;
            File teiFile = new File(TEIDICT_DIR.getPath() + File.separator + artikelXML.getLemma() + ".xml");
            if (teiFile.exists()) {
                cal.setTimeInMillis(teiFile.lastModified());
                previousConversion = LocalDateTime.ofInstant(cal.toInstant(), ZoneId.systemDefault());
            }
            if (logLemma)
                LOGGER.info(String.format("%s wijz: %s, fase: %s, prev: %s", artikelXML.getLemma(), artikelXML.getLaatste_wijziging(), artikelXML.getArtikelfasedatum(), previousConversion));
            File onfwFile = new File(ONFW_DIR.getPath() + File.separator + artikelXML.getLemma() + ".xml");
            if (!onfwFile.exists()) {
                try (
                        FileWriter onfwWriter = new FileWriter(onfwFile);
                ) {
                    onfwWriter.write(artikelXML.getXml());
                }
            }
/*
            bij artikelfasedatum staat de tijd altijd op 00:00.
            een conversie van 23-02 krijgt 24-02T06:35 als tijd
            een fasedatum wijziging op 24-02 krijgt 24-02T00:00 en wordt dus overgeslagen
            oplossing: als de datum hetzelfde is ook converteren
 */

            if (previousConversion == null || isAfterOrSameDate(artikelXML.getLaatste_wijziging(), previousConversion) || isAfterOrSameDate(artikelXML.getArtikelfasedatum(), previousConversion)) {
                if (Arrays.stream(statussen).anyMatch(s -> s == artikelXML.getArtikelfase())) {
                    /*
                    This artikel may be linking to others, these need to be processed as well for copying the information from it
                     */
                    artikelen.stream().filter(a->artikelXML.getId().equals(a.getLinked())).forEach(toProcess::add);
                    toProcess.add(artikelXML);
                }
            }
        }
        System.out.println(toProcess.size() + " to process");
        return toProcess;
    }

    @Override
    public void copyToDictionary(Stream<ToCopy> toCopy) {
        System.out.println(String.format(now() + " copying to %s and %s", TEIDICT_DIR.getPath(), TEIDICT_DIR_PROD.getPath()));
        final AtomicInteger copied = new AtomicInteger();
        toCopy.forEach(tc ->
                {
                    Path source = tc.getSource().toPath();
                    Path dest = TEIDICT_DIR.toPath().resolve(source.getFileName());
                    Path dest2 = TEIDICT_DIR_PROD.toPath().resolve(source.getFileName());
                    try (
                            FileChannel from = FileChannel.open(source, StandardOpenOption.READ);
                            FileChannel to = FileChannel.open(dest, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    ) {
                        from.transferTo(0, from.size(), to);
                        if (tc.getFase() != CANDIDATE) {
                            try (FileChannel to2 = FileChannel.open(dest2, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                            ) {
                                from.transferTo(0, from.size(), to2);
                            }
                        }
                        copied.incrementAndGet();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
        if (mailConversion && converted) mail("dws converted", publog.toString());
        System.out.println(now() + " done, copied " + copied);
    }

    @Override
    public Stream<ToCopy> transformToTEI(List<ArtikelXML> toProcess, File logFile) throws IOException, TransformerConfigurationException {
        System.out.println(now() + " converting to " + DATA_DIR);
        PrintStream err = System.err;
        System.out.println(now() + " writing transform messages to " + logFile.getPath());
        try (
                PrintStream xsltLog = new PrintStream(new BufferedOutputStream(new FileOutputStream(logFile, true)));
        ) {
            System.setErr(xsltLog);
            System.err.println("transform at " + currentPubDate);
            System.err.println("-----------------------------");
            // need a new factory, otherwise the new System.err isn't used
            XslTransformer transformer = new XslTransformer(new BufferedInputStream(new FileInputStream(xsltFile)), true);
            AtomicBoolean header = new AtomicBoolean(true);
            AtomicInteger numconverted = new AtomicInteger(toProcess.size());
            toProcess.forEach(artikelXML -> {
                transformer.clearParameters();
                voorredactie = "true";
                transformer.addParameter("voorredactie", voorredactie);
                transformer.addParameter("user", artikelXML.getUsername());
                transformer.addParameter("status", String.valueOf(artikelXML.getArtikelfase()));
                File teiFile = new File(DATA_DIR.getPath() + File.separator + artikelXML.getLemma() + ".xml");
                try (
                        FileWriter teiWriter = new FileWriter(teiFile);
                ) {
                    if (header.getAndSet(false)) {
                        transformer.addParameter("printheader", "true");
                    }
                    String tei = transformer.transform(artikelXML.getXml());
                    if (tei.contains("<ok xmlns=") || tei.isEmpty()) {
                        Files.deleteIfExists(teiFile.toPath());
                        numconverted.decrementAndGet();
                    } else {
                        try {
                            FA_RngValidationHelper.validateRngSchematron(tei);
                            teiWriter.write(tei);
                            if (mailConversion) {
                                LocalDateTime localDateTime = isAfterOrSameDate(artikelXML.getArtikelfasedatum(), artikelXML.getLaatste_wijziging()) ?
                                        artikelXML.getArtikelfasedatum().atStartOfDay() : artikelXML.getLaatste_wijziging();
                                publog.append(
                                        String.format("published %s, last changed: %s\n", artikelXML.getLemma(), localDateTime)
                                );
                                converted = true;
                            }
                        } catch (Exception e) {
                            numconverted.decrementAndGet();
                            Files.write(APP_DIR.toPath().resolve("invalid_" + teiFile.getName()), tei.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                            System.err.println(artikelXML.getLemma() + ": validatie fout:");
                            mail(" dws invalid " + artikelXML.getLemma(), PublishBean.asString(e));
                            Files.deleteIfExists(teiFile.toPath());
                        }
                    }
                } catch (IOException | TransformerException e) {
                    throw new RuntimeException(e);
                }
            });
            xsltLog.println(String.format("%s lemmas to convert: %d, converted: %d", LocalDateTime.now(), toProcess.size(), numconverted.get()));
            xsltLog.flush();
            // TODO below should not be needed, somehow these lines appear in the log
            // with no influence on the validation outcome
            File filtered = File.createTempFile("log", "filtered");
            filtered.deleteOnExit();
            Files.newBufferedReader(logFile.toPath()).lines().filter(l ->
                    !(l.trim().equals("Error") ||
                            l.startsWith("  Error reported by XML parser processing") ||
                            l.contains(".jar!/name/dmaus/schxslt/: Premature end of file"))
            ).forEach(l -> {
                try {
                    Files.write(filtered.toPath(), (l + System.lineSeparator()).getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            });
            if (filtered.length() > 0)
                Files.move(filtered.toPath(), logFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } finally {
            System.setErr(err);
        }
        return Stream.concat(Stream.of(new ToCopy(new File(DATA_DIR.getPath() + File.separator + "onfw_metadata.xml"), 2)),
                toProcess.stream()
                        .map(a ->
                                new ToCopy(getTarget(a), a.getArtikelfase()))
        ).filter(tc -> tc.getSource().exists());

    }

    private boolean isAfterOrSameDate(LocalDateTime first, LocalDateTime second) {
        return first != null && second != null ? first.isAfter(second) || first.toLocalDate().equals(second.toLocalDate()) : false;
    }

    private boolean isAfterOrSameDate(LocalDate first, LocalDateTime second) {
        return first != null && second != null ? first.isAfter(second.toLocalDate()) || first.equals(second.toLocalDate()) : false;
    }

    protected File getTarget(ArtikelXML a) {
        return new File(DATA_DIR.getPath() + File.separator + a.getLemma() + ".xml");
    }

}
