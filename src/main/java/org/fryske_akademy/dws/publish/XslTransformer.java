package org.fryske_akademy.dws.publish;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class XslTransformer {

    /**
     * Threadsafe as long as you don't change Configuration, which we don't. See
     * https://saxonica.plan.io/boards/2/topics/5645.
     */
    private static final TransformerFactory FACTORY
            = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", XslTransformer.class.getClassLoader());

    private final Map<String, String> params = new HashMap<>();

    private final Transformer transformer;

    private static final Map<String, Templates> TEMPLATES = new HashMap<>();

    /**
     * Constructs a new transformer (and templates if not cached and cache is enabled) and caches the templates it caching is enabled.
     *
     * @param id
     * @param source
     * @return
     * @throws TransformerConfigurationException
     */
    private static Transformer get(String id, StreamSource source, boolean newFactory) throws TransformerConfigurationException {
        boolean has = id != null && TEMPLATES.containsKey(id);

        synchronized (TEMPLATES) {
            if (has) {
                return TEMPLATES.get(id).newTransformer();
            }

            Templates t = newFactory ?
                    TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",
                            XslTransformer.class.getClassLoader()).newTemplates(source) :
                    FACTORY.newTemplates(source);
            if (id!=null) TEMPLATES.put(id, t);
            return t.newTransformer();
        }
    }

    public XslTransformer(File stylesheet) throws TransformerConfigurationException, FileNotFoundException {
        transformer = get(stylesheet.getAbsolutePath(), new StreamSource(stylesheet), false);
    }

    public XslTransformer(InputStream stylesheet, String id, boolean newFactory) throws TransformerConfigurationException {
        transformer = get(id, new StreamSource(stylesheet),newFactory);
    }

    public XslTransformer(InputStream stylesheet, boolean newFactory) throws TransformerConfigurationException {
        transformer = get(null, new StreamSource(stylesheet),newFactory);
    }

    public XslTransformer(Reader stylesheet, boolean newFactory) throws TransformerConfigurationException {
        transformer = get(null, new StreamSource(stylesheet),newFactory);
    }

    /**
     * stylesheet is assumed to be a resource URI
     *
     * @param stylesheet
     * @throws TransformerConfigurationException
     */
    public XslTransformer(String stylesheet) throws TransformerConfigurationException {
        transformer = get(stylesheet, new StreamSource(stylesheet),false);
    }

    public XslTransformer(String stylesheet, Reader sheet) throws TransformerConfigurationException {
        transformer = get(stylesheet, new StreamSource(sheet),false);
    }

    public String transform(String source)
            throws TransformerException {
        StreamSource ssSource = new StreamSource(new StringReader(source));
        StringWriter result = new StringWriter();
        StreamResult streamResult = new StreamResult(result);

        synchronized (transformer) {
            doTransform(ssSource, streamResult);
        }

        return result.toString();
    }

    private void doTransform(StreamSource ssSource, StreamResult streamResult) throws TransformerException {
        transformer.reset();
        transformer.clearParameters();
        for (Entry<String, String> e : params.entrySet()) {
            transformer.setParameter(e.getKey(), e.getValue());
        }

        transformer.transform(ssSource, streamResult);
    }

    public <W extends Writer> W streamTransform(Reader source, W result)
            throws TransformerException {
        StreamSource ssSource = new StreamSource(source);
        StreamResult streamResult = new StreamResult(result);

        synchronized (transformer) {
            doTransform(ssSource, streamResult);
        }
        return result;

    }

    public void addParameter(String key, String value) {
        params.put(key, value);
    }

    public void clearParameters() {
        params.clear();
    }

    public void setErrorListener(ErrorListener errorListener) {
        transformer.setErrorListener(errorListener);
    }
}
