package org.fryske_akademy.dws.publish;

import org.fryske_akademy.services.AbstractCrudService;
import org.fryske_akademy.services.CrudReadService;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

@Stateless
@Local(CrudReadService.class)
public class ExportService extends AbstractCrudService {

    private EntityManager em;

    @PostConstruct
    private void init() {
        EntityManagerFactory onfw_unit = Persistence.createEntityManagerFactory("onfw_unit");
        em = onfw_unit.createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }


}
