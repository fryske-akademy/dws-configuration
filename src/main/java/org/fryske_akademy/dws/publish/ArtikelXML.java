package org.fryske_akademy.dws.publish;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "artikelxml")
public class ArtikelXML {
    private Integer id;
    @Id
    private Integer pid;
    private String lemma;
    @Column(name = "tekst")
    private String xml;
    private int artikelfase;
    private String username;
    private LocalDateTime laatste_wijziging;
    private Integer linked;

    @Column
    private LocalDate artikelfasedatum;

    public String getLemma() {
        return lemma.replaceFirst("/.*","");
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public int getArtikelfase() {
        return artikelfase;
    }

    public void setArtikelfase(int artikelfase) {
        this.artikelfase = artikelfase;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getLaatste_wijziging() {
        return laatste_wijziging;
    }

    public void setLaatste_wijziging(LocalDateTime laatste_wijziging) {
        this.laatste_wijziging = laatste_wijziging;
    }

    public LocalDate getArtikelfasedatum() {
        return artikelfasedatum;
    }

    public void setArtikelfasedatum(LocalDate artikelfasedatum) {
        this.artikelfasedatum = artikelfasedatum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLinked() {
        return linked;
    }

    public void setLinked(Integer linked) {
        this.linked = linked;
    }
}
