package org.fryske_akademy.dws.publish;

import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Stream;

public interface PublishBean {

    public static class ToCopy {
        private final File source;
        private final int fase;

        public ToCopy(File source, int fase) {
            this.source = source;
            this.fase = fase;
        }

        public File getSource() {
            return source;
        }

        public int getFase() {
            return fase;
        }
    }

    // TODO transform logger must word with its own datadir
    /**
     * data are transformed here, dir will be cleared and created if it doesn't exist
     */
    public static final File DATA_DIR = new File("/data");
    /**
     * data are downloaded here from database, dir will be cleared and created if it doesn't exist
     */
    public static final File ONFW_DIR = new File("/onfw");
    /**
     * docker volume to teidict data
     */
    public static final File TEIDICT_DIR = new File("/teidictdata");
    /**
     * docker volume to teidict data for production
     */
    public static final File TEIDICT_DIR_PROD = new File("/teidictdataprod");
    /**
     * docker volume to app data
     */
    public static final File APP_DIR = new File("/dwspublish");
    public static final String DATEFORMAT = "yyyy-MM-dd_HH:mm";
    /**
     * name of the transformation xslt in {@link #APP_DIR}.
     */
    String DWS_TO_TEI = File.separator + "dwsToTEI.xslt";

    public static String asString(Throwable throwable) {
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }
    /**
     * prepare (empty) directories for data collection and transformation
     * @return
     */
    void init();

    /**
     * collect data from dws, return the articles to transform
     * @return
     * @throws IOException
     */
    List<ArtikelXML> copyFromDws() throws IOException;

    /**
     * transform articles to {@link PublishBean#DATA_DIR}, log to the given file
     * @param toProcess
     * @param logFile
     * @throws IOException
     * @throws TransformerConfigurationException
     */
    Stream<ToCopy> transformToTEI(List<ArtikelXML> toProcess, File logFile) throws IOException, TransformerConfigurationException;

    /**
     * copy transformed xml from {@link PublishBean#DATA_DIR} to {@link PublishBean#TEIDICT_DIR} and {@link PublishBean#TEIDICT_DIR_PROD}
     * @throws IOException
     */
    void copyToDictionary(Stream<ToCopy> toCopy);

    void mail(String msg, String body);

}
