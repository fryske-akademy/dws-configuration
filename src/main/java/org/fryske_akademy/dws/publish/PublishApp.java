package org.fryske_akademy.dws.publish;

import com.vectorprint.configuration.cdi.CDIProperties;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.JpqlBuilder;
import org.fryske_akademy.jpa.JpqlBuilderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

public class PublishApp {

    public static final Logger LOGGER = LoggerFactory.getLogger(PublishApp.class);

    /**
     * Reads artikel xml from database into dir "onfw", transforms xml to TEI
     * xml into dir "data", copy TEI xml into dictionary data dir mounted on
     * /teidictdata and to a data dir mounted on teidictdataprod.
     *
     * @param args not used
     */
    public static void main(String[] args) {
        final SeContainerInitializer seContainerInitializer = SeContainerInitializer.newInstance().addPackages(
                PublishApp.class.getPackage(),
                JpqlBuilder.class.getPackage(),
                CDIProperties.class.getPackage()
        ).addBeanClasses(ExportService.class, JpqlBuilderImpl.class);

        Instance<PublishBean> publishBeanInst = null;
        try (SeContainer seContainer = seContainerInitializer.initialize()) {
            publishBeanInst = seContainer.select(PublishBean.class);
            List<ArtikelXML> artikelXMLS;
            PublishBean publishBean = publishBeanInst.get();
            publishBean.init();
            artikelXMLS = publishBean.copyFromDws();
            File logFile = new File(PublishBean.APP_DIR + File.separator + "publishlog.txt");
            Stream<PublishBean.ToCopy> copyStream = publishBean.transformToTEI(artikelXMLS, logFile);
            publishBean.copyToDictionary(copyStream);
        } catch (Exception e) {
            LOGGER.error("failure during publishing", Util.deepestCause(e));
            File logFile = new File(PublishBean.APP_DIR + File.separator + "publishlog.txt");
            try {
                Files.writeString(logFile.toPath(), LocalDateTime.now() + ": ERROR DURING PUBLISHING, sending mail to admin\n", StandardOpenOption.APPEND);
            } catch (IOException ex) {
            }
            publishBeanInst.get().mail("failure during dws publish", PublishBean.asString(Util.deepestCause(e)));
        }

    }

}
