package org.fryske_akademy.dws.dwsimport;

//import nl.inl.anw.ArtikelFatalException;

import java.io.File;
import java.io.IOException;

public interface ImportBean {

    void importNfw(File artikels) throws IOException;
//    void importNfw(File artikels) throws ArtikelFatalException, IOException;
}
