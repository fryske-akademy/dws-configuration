package org.fryske_akademy.dws.dwsimport;


import java.io.File;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

@ApplicationScoped
@Named
public class ImportBeanImpl implements ImportBean {

    @Override
    public void importNfw(File artikels) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
//    public static final Logger LOGGER = LoggerFactory.getLogger(ImportBeanImpl.class);
//
//    @Inject
//    @Property
//    private String fkwlemmaurl;
//
//    private static final String UNCHECKED = "?onlyChecked=false";
//    private static final String SENSITIVE = "&insensitive=false";
//
//    @Inject
//    @Property
//    private String nfwurl;
//
//    private NFWGetParadigm nfwGetParadigm = new NFWGetParadigm(null, null);
//
//    @Override
//    public void importNfw(File artikels) throws ArtikelFatalException, IOException {
//        ArtikelDatabase database = ArtikelDatabase.getConnection();
//        AnwArticle.setIgnoreLocking(true);
//        ResultSet artikelen = database.getLijstArtikelen(ArticleSelection.all()).resultSet();
//        ArtikelFase artikelFase = ArtikelDatabase.getArtikelFase("neo-lijst");
//        User user = ArtikelDatabase.getUser("import");
//        if (user==null)
//            throw new IllegalStateException("user \"import\" not found");
//        Files.lines(new File("importonfw.txt").toPath(), StandardCharsets.UTF_8).forEach(lemma -> {
//            try {
//                /*
//                -* vertaling(en) + pos uit nfwb
//                -* lemma graph(s) uit fkw
//                -* kies lemmagraph
//                  - hfdletter => zoek propn
//                  - eerste pos match
//                - zoek of maak anw (1 keer voor alle vertalingen)
//                - check randvoorwaarden
//                - vul anw aan met vertaling(en) en hyph/article
//                - status en user
//                 */
//                HttpRequest request = HttpRequest.newBuilder().GET()
//                        .uri(URI.create(nfwurl + URLEncoder.encode(lemma, StandardCharsets.UTF_8)))
//                        .setHeader("User-Agent", "Java 11 HttpClient").build();
//                String json = request(request);
//                ObjectMapper om = new ObjectMapper();
//                JsonNode nfwTranslations = om.readTree(json);
//                if (!json.contains("\"tr\":")) {
//                    LOGGER.warn("skipping lemma: no translation in nfwb for: " + lemma);
//                    return;
//                }
//                String pos = nfwTranslations.findValue("pos").textValue();
//                List<JsonNode> translations = new ArrayList<>();
//
//                List<String> trs = getValues(nfwTranslations.findValue("tr"));
//
//                for (String tr : trs) {
//                    if (tr.isEmpty()) continue;
//                    // try to add translation by looking for preferred lemma in fkw, possibly return propn pos
//                    pos = pickLemma(pos, translations, lemma, tr, trs, om, false);
//                }
//
//                if (translations.isEmpty()) {
//                    LOGGER.info(String.format("skipping lemma: no translations for %s", lemma));
//                    artikelen.beforeFirst();
//                    return;
//                }
//
//                List<JsonNode> ll = new ArrayList<>();
//
//                for (int i = 0; i < translations.size(); i++) {
//                    if (i==0) {
//                        ll.add(translations.get(0));
//                        continue;
//                    }
//                    if (!translations.get(i).get("form").textValue().equals(translations.get(i-1).get("form").textValue())) {
//                        ll.add(translations.get(i));
//                    }
//                }
//                translations=ll;
//
//                AnwArticle anwArticle = null;
//                InfoTree artikelTree = null;
//                String startUpperCase = Character.toUpperCase(lemma.charAt(0)) + lemma.substring(1);
//                while (artikelen.next()) {
//                    if (artikelen.getString(2).equals(lemma)) {
//                        anwArticle = new AnwArticle(database, lemma);
//                        artikelTree = anwArticle.getInfoTree(database);
//                        if (artikelTree.selectElements("artikel/Woordsoort").isEmpty()) {
//                            addPos(pos, artikelTree.getRootElement());
//                        } else {
//                            String dwspos = getPos(artikelTree);
//                            if (!dwspos.isEmpty()) pos = dwspos;
//                        }
//                        LOGGER.info("found in dws: " + lemma);
//                        break;
//                    } else if (artikelen.getString(2).equals(startUpperCase)) {
//                        anwArticle = new AnwArticle(database, startUpperCase);
//                        artikelTree = anwArticle.getInfoTree(database);
//                        if (artikelTree.selectElements("artikel/Woordsoort").isEmpty()) {
//                            addPos(pos, artikelTree.getRootElement());
//                        } else {
//                            String dwspos = getPos(artikelTree);
//                            if (!dwspos.isEmpty()) pos = dwspos;
//                        }
//                        LOGGER.info("found with uppercase in dws: " + lemma);
//                        break;
//                    }
//                }
//                if (anwArticle != null) {
//                    Element ws = artikelTree.selectSingleElement("artikel/Woordsoort");
//                    if (ws == null) {
//                        LOGGER.warn(String.format("skipping lemma: no Woordsoort in dws for %s", lemma));
//                        anwArticle = null;
//                    } else {
//                        String anwpos = getPos(artikelTree);
//                        if (!anwpos.isEmpty() && !translations.stream().allMatch(tr -> {
//                            if (tr instanceof JsonNode) {
//                                return anwpos.equals(((JsonNode) tr).get("linguistics").textValue().toLowerCase(Locale.ROOT));
//                            } else return true;
//                        })) {
//                            LOGGER.warn(String.format("skipping lemma: pos mismatch in fkw translation(s) for %s", lemma));
//                            anwArticle = null;
//                        }
//                    }
//                }
//                if (anwArticle != null) processArticle(anwArticle, artikelTree, database, translations, pos, artikelFase, user);
//                artikelen.beforeFirst();
//            } catch (Exception e) {
//                LOGGER.error("fout bij " + lemma, e);
//                throw new RuntimeException(e);
//            }
//        });
//
//    }
//
//    private List<JsonNode> preferred(JsonNode lemmas, boolean preferred) {
//        Iterator<JsonNode> elements = lemmas.get("Lemma").elements();
//        List<JsonNode> rv = new ArrayList<>();
//        while (elements.hasNext()) {
//            JsonNode l = elements.next();
//            if (l.get("preferred").booleanValue()==preferred) rv.add(l);
//        }
//        return rv;
//    }
//
//    private String pickLemma(String pos, List<JsonNode> translations, String lemma, String tr, List<String> trs, ObjectMapper om, boolean sensitive) throws ExecutionException, InterruptedException, JsonProcessingException, TimeoutException {
//        JsonNode fromFkw = getFromFkw(tr, om, sensitive);
//        if (fromFkw == null) return "";
//        if (fromFkw.get("numresults").intValue()==0) {
//            LOGGER.info(String.format("not in fkw: %s → %s",lemma, tr));
//            return pos;
//        }
//        List<JsonNode> preferred = preferred(fromFkw, true);
//        List<JsonNode> elements = preferred.isEmpty() ? preferred(fromFkw, false) : preferred;
//
//        for (JsonNode l : elements) {
//            if (!l.get("preferred").booleanValue()) {
//                LOGGER.info(String.format("skipping translation: non preferred " + l.get("form").textValue()));
//                if (!l.has("standard_lemma")||trs.contains(l.get("standard_lemma").textValue())) {
//                    continue;
//                } else {
//                    LOGGER.info(String.format("adding preferred from fkw " + l.get("standard_lemma").textValue()));
//                    return pickLemma(pos, translations,lemma,l.get("standard_lemma").textValue(),trs,om, true);
//                }
//            }
//            if (l.get("linguistics").textValue().contains("PROPN") && pos.equals("pos.noun")) {
//                pos = l.get("linguistics").textValue().toLowerCase(Locale.ROOT);
//                translations.add(l);
//                break;
//            } else if (pos.equals(l.get("linguistics").textValue().toLowerCase(Locale.ROOT))) {
//                translations.add(l);
//                break;
//            } else {
//                LOGGER.warn(String.format("skipping translation: no matching pos %s ←→ %s for %s", l.get("linguistics").textValue(), pos, tr));
//            }
//        }
//        return pos;
//    }
//
//    private JsonNode getFromFkw(String tr, ObjectMapper om, boolean sensitive) throws ExecutionException, InterruptedException, TimeoutException, JsonProcessingException {
//        HttpRequest request = HttpRequest.newBuilder().GET()
//                .uri(URI.create(fkwlemmaurl + URLEncoder.encode(tr, StandardCharsets.UTF_8)
//                        + UNCHECKED
//                + (sensitive ? SENSITIVE : "")))
//                .setHeader("User-Agent", "Java 11 HttpClient").build();
//        String json = request(request);
//        if ("failed".equals(json)) {
//            LOGGER.warn(String.format("skipping translation: fkw request failed for %s", tr));
//            return null;
//        }
//        return om.readTree(json);
//    }
//
//    private String getPos(Element artikelTree) {
//        return "pos." + nfwGetParadigm.getStdwHelper().convertPos(artikelTree.selectSingleElement("artikel/Woordsoort")).substring(1);
//    }
//
//    private List<String> getValues(JsonNode node) {
//        List<String> rv = new ArrayList<>(3);
//        if (node instanceof ArrayNode) {
//            ((ArrayNode) node).elements().forEachRemaining(e -> rv.add(e.textValue()));
//        } else if (node instanceof TextNode) {
//            rv.add(node.textValue());
//        } else {
//            throw new IllegalArgumentException("not supported: " + node.getClass().getName());
//        }
//        return rv;
//    }
//
//    private void addPos(String pos, Element element) {
//        switch (pos) {
//            case "pos.noun":
//            case "pos.propn":
//                Element woordsoort = element
//                        .appendToRightSlot("Woordsoort");
//                woordsoort.appendToRightSlot("Type", "substantief");
//                if (pos.equals("pos.propn")) {
//                    woordsoort
//                            .appendToRightSlot("Substantief")
//                            .appendToRightSlot("Naamtype", "eigennaam");
//                } else {
//                    woordsoort
//                            .appendToRightSlot("Substantief")
//                            .appendToRightSlot("Naamtype", "soortnaam");
//                }
//                break;
//            case "pos.verb":
//            case "pos.aux":
//                woordsoort = element
//                        .appendToRightSlot("Woordsoort");
//                woordsoort.appendToRightSlot("Type", "werkwoord");
//                if (pos.equals("pos.verb")) {
//                    woordsoort
//                            .appendToRightSlot("Werkwoord")
//                            .appendToRightSlot("Functie", "zelfstandig werkwoord");
//                } else {
//                    woordsoort
//                            .appendToRightSlot("Werkwoord")
//                            .appendToRightSlot("Functie", "hulpwerkwoord");
//                }
//                break;
//            case "pos.pron":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "voornaamwoord");
//                break;
//            case "pos.adj":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "adjectief");
//                break;
//            case "pos.adp":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "voorzetsel");
//                break;
//            case "pos.adv":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "bijwoord");
//                break;
//            case "pos.num":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "telwoord");
//                break;
//            case "pos.cconj":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "voegwoord");
//                break;
//            case "pos.intj":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "tussenwerpsel");
//                break;
//            case "pos.det":
//                element
//                        .appendToRightSlot("Woordsoort")
//                        .appendToRightSlot("Type", "lidwoord");
//                break;
//        }
//    }
//
//    private void processArticle(AnwArticle anwArticle, InfoTree artikelTree, ArtikelDatabase database, List<JsonNode> translations, String pos
//    , ArtikelFase artikelFase, User user) throws Exception {
//        /*
//        overslaan: koepel, meer betekenissen, bestaande oersetting
//         */
//        if (anwArticle.getType(database) == AnwArticle.Type.KOEPELARTIKEL) {
//            LOGGER.warn("skipping lemma: koepel in onfw for " + anwArticle.getLemma());
//        } else {
//            Element rootElement = artikelTree.getRootElement();
//            List<Element> senses = rootElement.selectElements("//betekenisInfo");
//            if (senses.size() > 1) {
//                LOGGER.warn("skipping lemma: more senses in onfw for " + anwArticle.getLemma());
//            } else {
//                List<Element> oersettingen = rootElement.selectElements("//Oersetting");
//                boolean hasFoarm = false;
//                oer:
//                for (Element oersetting : oersettingen) {
//                    for (Element foarm : oersetting.getDescendantsByName("Foarm")) {
//                        if (!"---".equals(foarm.getValue())) {
//                            hasFoarm = true;
//                            break oer;
//                        }
//                    }
//                }
//                if (hasFoarm) {
//                    LOGGER.warn("skipping lemma: existing translation for " + anwArticle.getLemma());
//                } else {
//                    if (senses.isEmpty()) {
//                        Element betekenisInfo = rootElement
//                                .appendToRightSlot("BetekenisEnGebruik").selectSingleElement(".//betekenisInfo");
//                        betekenisInfo.appendToRightSlot("Oersettingen");
//                    }
//                    addFrysk(anwArticle, rootElement.selectSingleElement(".//betekenisInfo"), database, translations, pos);
//
//                    MetadataTop metadataTop = anwArticle.getMetadataTop(database);
//                    metadataTop.artikelfase = artikelFase;
//                    metadataTop.redacteur = user;
//                    anwArticle.putMetadataTop(database, metadataTop);
//                    anwArticle.putInfoTree(database, artikelTree);
//                    LOGGER.info("completed: " + anwArticle.getLemma() + " → " +
//                            translations.stream().map(tr -> tr.get("form").textValue()).collect(Collectors.joining(", "))
//                    );
//                }
//            }
//        }
//    }
//
//    private void addFrysk(AnwArticle anwArticle, Element betekenisInfo, ArtikelDatabase database, List<JsonNode> translations, String pos) throws Exception {
//        for (Element oersettingen : betekenisInfo.selectElements("//Oersettingen")) {
//            oersettingen.getParentElement().remove(oersettingen);
//        }
//        if (betekenisInfo.getDescendantsByName("Oersettingen").isEmpty()) {
//            betekenisInfo.appendToRightSlot("Oersettingen");
//        }
//        if (translations.isEmpty()) {
//            Element oersetting = betekenisInfo.getSingleChildElement("Oersettingen")
//                    .appendToRightSlot("Oersetting").appendToRightSlot("Foarm", "---");
//            return;
//        }
//        LOGGER.info("adding translations to " + anwArticle.getLemma());
//        translations.forEach(tr -> {
//            Element oersetting = betekenisInfo.getSingleChildElement("Oersettingen")
//                    .appendToRightSlot("Oersetting");
//            for (Element ws : oersetting.selectElements("Woordsoort"))
//                oersetting.remove(ws);
//            addPos(pos, oersetting);
//            if (pos.equals("pos.noun") && tr.has("article")) {
//                oersetting.selectElements("Woordsoort/Substantief")
//                        .get(0).appendToRightSlot("Lidwoord").appendToRightSlot("AardLidwoord")
//                        .setValue(tr.get("article").textValue());
//            }
//            oersetting.appendToRightSlot("Foarm", tr.get("form").textValue());
//            try {
//                nfwGetParadigm.addParadigmInfo(oersetting, false, tr);
//            } catch (InfoTreeValidationException e) {
//                LOGGER.warn(String.format("failed to add paradigm for " + tr.get("form").textValue()), e);
//            }
//        });
//    }
//
//    private static final Pattern STARTCAPITAL = Pattern.compile("^[A-Z]");
//
//
//    @Inject
//    @Property(defaultValue = "30000")
//    private int timeoutSeconds;
//
//    private static final HttpClient httpClient = HttpClient.newBuilder().build();
//
//    protected String request(HttpRequest request) throws TimeoutException, ExecutionException, InterruptedException {
//        try {
//            HttpResponse<String> response = httpClient
//                    .sendAsync(request, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8))
//                    .get(timeoutSeconds, TimeUnit.SECONDS);
//            if (response.statusCode() == HttpURLConnection.HTTP_OK) {
//                return response.body();
//            } else {
//                LOGGER.warn(String.format("%s failed: %d", request.uri().toString(), response.statusCode()));
//                return "failed";
//            }
//        } catch (InterruptedException | ExecutionException | TimeoutException e) {
//            throw e;
//        }
//
//    }
}
