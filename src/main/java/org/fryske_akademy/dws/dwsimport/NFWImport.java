package org.fryske_akademy.dws.dwsimport;

import com.vectorprint.configuration.cdi.CDIProperties;
//import nl.inl.anw.ArtikelFatalException;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.JpqlBuilder;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import java.io.File;
import java.io.IOException;

public class NFWImport {


    public static void main(String[] args) throws IOException {
//    public static void main(String[] args) throws IOException, ArtikelFatalException {
        final SeContainerInitializer seContainerInitializer = SeContainerInitializer.newInstance()
                .disableDiscovery().addPackages(
                        NFWImport.class.getPackage(),
                        JpqlBuilder.class.getPackage(),
                        CDIProperties.class.getPackage()
                );

        try (SeContainer seContainer = seContainerInitializer.initialize();
        ) {
            Util.getBean(ImportBean.class,"importBeanImpl").importNfw(new File("importnfw.txt"));
        }
    }
}