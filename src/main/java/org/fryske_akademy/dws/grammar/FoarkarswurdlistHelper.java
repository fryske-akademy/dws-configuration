/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fryske_akademy.dws.grammar;

import java.util.*;

/**
 * NOTE this class is managed here and only here: https://bitbucket.org/fryske-akademy/dws-configuration!!!
 * @author eduard
 */
public class FoarkarswurdlistHelper {

    // universal terms
    public static final String POSNOUN = "pos.noun";
    public static final String POSDET = "pos.det";
    public static final String POSPROPN = "pos.propn";
    public static final String POSINTJ = "pos.intj";
    public static final String POSCCONJ = "pos.cconj";
    public static final String POSSCONJ = "pos.sconj";
    public static final String POSADP = "pos.adp";
    public static final String POSADV = "pos.adv";
    public static final String POSNUM = "pos.num";
    public static final String POSPRON = "pos.pron";
    public static final String POSPART = "pos.part";
    public static final String PRONTYPEART = "prontype.art";
    public static final String PRONTYPEPRS = "prontype.prs";
    public static final String POLITEINFM = "polite.infm";
    public static final String POLITEFORM = "polite.form";
    public static final String POSADJ = "pos.adj";
    public static final String POSVERB = "pos.verb";
    public static final String POSAUX = "pos.aux";
    public static final String POSSYM = "pos.sym";
    public static final String POSPUNCT = "pos.punct";
    public static final String TELWOORD = "telwoord";
    public static final String LIDWOORD = "lidwoord";
    public static final String ISLEMMA = "islemma.yes";
    public static final String POSSESSIVE = "poss.yes";
    public static final String DEGREEDIM = "degree.dim";
    public static final String NUMTYPEORD = "numtype.ord";
    public static final String NUMTYPECARD = "numtype.card";
    public static final String TENSEPAST = "tense.past";
    public static final String TENSEPRES = "tense.pres";
    public static final String NUMBERSING = "number.sing";
    public static final String NUMBERPLUR = "number.plur";
    public static final String NUMBERPTAN = "number.ptan";
    public static final String PERSON3 = "person.3";
    public static final String PERSON2 = "person.2";
    public static final String PERSON1 = "person.1";
    public static final String VERBFORMPART = "verbform.part";
    public static final String VERBFORMINF = "verbform.inf";
    public static final String VERBFORMGER = "verbform.ger";
    public static final String CASEPAR = "case.par";
    public static final String CASENOM = "case.nom";
    public static final String CASEACC = "case.acc";
    public static final String DEGREEPOS = "degree.pos";
    public static final String DEGREESUP = "degree.sup";
    public static final String DEGREECMP = "degree.cmp";
    public static final String INFLECTIONINFL = "inflection.infl";
    public static final String INFLECTIONUNINF = "inflection.uninf";
    public static final String PREDICATEYES = "predicate.yes";
    public static final String CONSTRUCTIONATTR = "construction.attr";
    public static final String CLITICYES = "clitic.yes";

    // nfw terms
    public static final String SUBSTANTIEF = "substantief";
    public static final String OVERIGE = "overige";
    public static final String TUSSENWERPSEL = "tussenwerpsel";
    public static final String VOEGWOORD = "voegwoord";
    public static final String VOORZETSEL = "voorzetsel";
    public static final String BIJWOORD = "bijwoord";
    public static final String BIJWOORDVORM = "BijwoordVorm";
    public static final String VOORNAAMWOORD = "voornaamwoord";
    public static final String ADJECTIEF = "adjectief";
    public static final String WERKWOORD = "werkwoord";
    public static final String TWEEDE_PERSOON_OTT = "Tweede_persoon_o.t.t";
    public static final String INFINITIEF = "Infinitief";
    public static final String OVERIGEVORM = "OverigeVorm";
    public static final String VOORZETSELVORM = "VoorzetselVorm";
    public static final String LIDWOORDVORM = "LidwoordVorm";
    public static final String BREUKGETAL = "Breukgetal";
    public static final String RANGTELWOORD = "Rangtelwoord";
    public static final String HOOFDTELWOORD = "Hoofdtelwoord";
    public static final String TUSSENWERPSELVORM = "TussenwerpselVorm";
    public static final String VOEGWOORDVORM = "VoegwoordVorm";
    public static final String OBJECTSVORM = "Objectsvorm";
    public static final String SUBJECTSVORM = "Subjectsvorm";
    public static final String ZELFSTANDIG_GEBRUIK_OP_S = "Zelfstandig_gebruik_op_s";
    public static final String OVERTREFFENDE_TRAP_VERBOGEN = "Overtreffende_trap_verbogen";
    public static final String OVERTREFFENDE_TRAP_ONVERBOGEN = "Overtreffende_trap_onverbogen";
    public static final String VERGROTENDETRAP_VERBOGEN = "Vergrotende-trap_verbogen";
    public static final String VERGROTENDE_TRAP_ONVERBOGEN = "Vergrotende_trap_onverbogen";
    public static final String STELLENDE_TRAP_VERBOGEN = "Stellende_trap_verbogen";
    public static final String STELLENDE_TRAP_ONVERBOGEN = "Stellende_trap_onverbogen";
    public static final String MEERVOUD = "Meervoud";
    public static final String VERKLEININGSVORM = "Verkleiningsvorm";
    public static final String EIGENNAAM = "Eigennaam";
    public static final String ENKELVOUD = "Enkelvoud";
    public static final String DOELFOARM = "doelfoarm";
    public static final String VOLTOOID_DEELWOORD = "Voltooid_deelwoord";
    public static final String TEGENWOORDIG_DEELWOORD = "Tegenwoordig_deelwoord";
    public static final String MEERVOUD_OVT = "Meervoud_o.v.t.";
    public static final String ENKELVOUD_OVT = "Enkelvoud_o.v.t";
    public static final String TWADDE_PERSOAN_DT = "twadde_persoan_dt.";
    public static final String EARSTE_EN_TREDDE_PERSOAN_DT = "earste_en_tredde_persoan_dt.";
    public static final String MEERVOUD_OTT = "Meervoud_o.t.t.";
    public static final String DERDE_PERSOON_OTT = "Derde_persoon_o.t.t";
    public static final String TWEEDE_PERSOON_OTT_BELEEFDHEID = "Tweede_persoon_o.t.t_beleefdheid";
    public static final String TWEEDE_PERSOON_OVT_BELEEFDHEID = "Tweede_persoon_o.v.t_beleefdheid";
    public static final String TWEEDE_PERSOON_OTT_INVERSIE = "Tweede_persoon_o.t.t_inversie";
    public static final String TWEEDE_PERSOON_OVT_INVERSIE = "Tweede_persoon_o.v.t_inversie";
    public static final String EERSTE_PERSOON_OTT = "Eerste_persoon_o.t.t.";

    public static boolean oneItemContains(Collection<String> linguistics, String... term) {
        return !findMatching(linguistics, term).isEmpty();
    }

    public static boolean oneItemContainsExcluded(String[] excludes, Collection<String> linguistics, String... term) {
        String matching = findMatching(linguistics, term);
        return !matching.isEmpty() && Arrays.stream(excludes).noneMatch(matching::contains);
    }

    /**
     * Find the first String in the argument list matching all the given terms
     * @param linguistics
     * @param term
     * @return
     */
    public static String findMatching(Collection<String> linguistics, String... term) {
        List<String> terms = Arrays.asList(term);
        boolean nullfoarm = terms.stream().anyMatch(ISLEMMA::equals);
        Optional<String> first = linguistics.stream().filter(element -> terms.stream().allMatch(element::contains)).findFirst();
        return nullfoarm && first.isPresent() ? "nullfoarm" : first.orElse("");
    }

    /**
     * is a linguistic category in NFW applicable given a list of universal
     * linguistic tags
     *
     * @param nfwLinguistics
     * @param universalLing
     * @return
     */
    public static boolean isNfwCategoryApplicable(String nfwLinguistics, Collection<String> universalLing) {
        switch (nfwLinguistics) {
            case INFINITIEF:
                return oneItemContains(universalLing,VERBFORMINF,ISLEMMA);
            case EERSTE_PERSOON_OTT:
                return oneItemContains(universalLing, PERSON1, TENSEPRES, NUMBERSING);
            case TWEEDE_PERSOON_OTT:
                String match = findMatching(universalLing, PERSON2, TENSEPRES, NUMBERSING);
                return !match.isEmpty() && !match.contains(CLITICYES);
            case DERDE_PERSOON_OTT:
                return oneItemContains(universalLing, PERSON3, TENSEPRES, NUMBERSING);
            case MEERVOUD_OTT:
                return oneItemContains(universalLing, VERBFORMINF) ||
                        oneItemContains(universalLing, PERSON1, TENSEPRES, NUMBERPLUR);
            case EARSTE_EN_TREDDE_PERSOAN_DT:
                return oneItemContains(universalLing, PERSON1, TENSEPAST, NUMBERSING)
                        && oneItemContains(universalLing, PERSON3, TENSEPAST, NUMBERSING);
            case TWADDE_PERSOAN_DT:
                match = findMatching(universalLing, PERSON2, TENSEPAST, NUMBERSING);
                return !match.isEmpty() && !match.contains(CLITICYES);
            case MEERVOUD_OVT:
                return oneItemContains(universalLing, NUMBERPLUR, TENSEPAST);
            case ENKELVOUD_OVT:
                return oneItemContains(universalLing, NUMBERSING, TENSEPAST);
            case VOLTOOID_DEELWOORD:
                return oneItemContains(universalLing, VERBFORMPART, TENSEPAST);
            case TEGENWOORDIG_DEELWOORD:
                return oneItemContains(universalLing, VERBFORMPART, TENSEPRES);
            case TWEEDE_PERSOON_OTT_INVERSIE:
                return oneItemContains(universalLing, CLITICYES, PERSON2, TENSEPRES);
            case TWEEDE_PERSOON_OVT_INVERSIE:
                return oneItemContains(universalLing, CLITICYES, PERSON2, TENSEPAST);
            case DOELFOARM:
                return oneItemContains(universalLing, VERBFORMGER);
            case TWEEDE_PERSOON_OTT_BELEEFDHEID:
                return oneItemContains(universalLing, PERSON2, TENSEPRES, NUMBERSING, POLITEFORM) ||
                        oneItemContains(universalLing, VERBFORMINF);
            case TWEEDE_PERSOON_OVT_BELEEFDHEID:
                return oneItemContains(universalLing, PERSON2, NUMBERPLUR, TENSEPAST) ||
                        oneItemContains(universalLing, PERSON2, NUMBERSING, TENSEPAST, POLITEFORM);
            case HOOFDTELWOORD:
                return oneItemContains(universalLing, NUMTYPECARD);
            case RANGTELWOORD:
                return oneItemContains(universalLing, NUMTYPEORD);
            case ENKELVOUD:
                return oneItemContains(universalLing,NUMBERSING,ISLEMMA);
            case MEERVOUD:
                return oneItemContainsExcluded(new String[]{DEGREEDIM}, universalLing, NUMBERPLUR) ||
                        oneItemContainsExcluded(new String[]{DEGREEDIM}, universalLing, NUMBERPTAN);
            case VERKLEININGSVORM:
                return oneItemContainsExcluded(new String[]{NUMBERPLUR}, universalLing, DEGREEDIM);
            case STELLENDE_TRAP_ONVERBOGEN:
                return oneItemContainsExcluded(new String[]{DEGREESUP, DEGREECMP},universalLing,INFLECTIONUNINF,PREDICATEYES);
            case STELLENDE_TRAP_VERBOGEN:
                return oneItemContainsExcluded(new String[]{DEGREESUP, DEGREECMP}, universalLing, NUMBERSING, POSNOUN, INFLECTIONINFL);
            case VERGROTENDE_TRAP_ONVERBOGEN:
                return oneItemContains(universalLing, DEGREECMP, INFLECTIONUNINF);
            case VERGROTENDETRAP_VERBOGEN:
                return oneItemContains(universalLing, DEGREECMP, INFLECTIONINFL);
            case OVERTREFFENDE_TRAP_ONVERBOGEN:
                return oneItemContains(universalLing, DEGREESUP, INFLECTIONUNINF);
            case OVERTREFFENDE_TRAP_VERBOGEN:
                return oneItemContains(universalLing, DEGREESUP, INFLECTIONINFL);
            case ZELFSTANDIG_GEBRUIK_OP_S:
                return oneItemContainsExcluded(new String[]{DEGREECMP, DEGREESUP}, universalLing, CASEPAR);
            case SUBJECTSVORM:
                return oneItemContains(universalLing, POSPRON, PRONTYPEPRS, CASENOM);
            case OBJECTSVORM:
                return oneItemContains(universalLing, POSPRON, PRONTYPEPRS, CASEACC);
            case BREUKGETAL:
        }
//        MessageBox.warning(null, String.format("Kan niet bepalen of %s van toepassing is op basis van %s", nfwLinguistics, universalLing));
        return false;
    }

    public static List<String> toUniversal(String nfwLinguistics) {
        List<String> rv = new ArrayList<>(3);
        switch (nfwLinguistics) {
            case INFINITIEF:
                rv.add(POSVERB);rv.add(VERBFORMINF);break;
            case EERSTE_PERSOON_OTT:
                rv.add(POSVERB);rv.add(PERSON1);rv.add(TENSEPRES);rv.add(NUMBERSING); break;
            case TWEEDE_PERSOON_OTT:
                rv.add(POSVERB);rv.add(PERSON2);rv.add(TENSEPRES);rv.add(NUMBERSING);break;
            case DERDE_PERSOON_OTT:
                rv.add(POSVERB);rv.add(PERSON3);rv.add(TENSEPRES);rv.add(NUMBERSING);break;
            case MEERVOUD_OTT:
                rv.add(POSVERB);rv.add( VERBFORMINF);rv.add(PERSON1);rv.add(TENSEPRES);rv.add( NUMBERPLUR);break;
            case EARSTE_EN_TREDDE_PERSOAN_DT:
                rv.add(POSVERB);rv.add(PERSON1);rv.add(TENSEPAST);rv.add(NUMBERSING);rv.add(PERSON3);break;
            case TWADDE_PERSOAN_DT:
                rv.add(POSVERB);rv.add(PERSON2);rv.add(TENSEPAST);rv.add(NUMBERSING);break;
            case MEERVOUD_OVT:
                rv.add(POSVERB);rv.add( NUMBERPLUR);rv.add(TENSEPAST);break;
            case ENKELVOUD_OVT:
                rv.add(POSVERB);rv.add( NUMBERSING);rv.add(TENSEPAST);break;
            case VOLTOOID_DEELWOORD:
                rv.add(POSVERB);rv.add( VERBFORMPART);rv.add(TENSEPAST);rv.add(INFLECTIONUNINF);break;
            case TEGENWOORDIG_DEELWOORD:
                rv.add(POSVERB);rv.add( INFLECTIONUNINF);rv.add(VERBFORMPART);rv.add(TENSEPRES);break;
            case TWEEDE_PERSOON_OTT_INVERSIE:
                rv.add(POSVERB);rv.add(CLITICYES);rv.add(PERSON2);rv.add(TENSEPRES);break;
            case TWEEDE_PERSOON_OVT_INVERSIE:
                rv.add(POSVERB);rv.add(CLITICYES);rv.add(PERSON2);rv.add(TENSEPAST);break;
            case DOELFOARM:
                rv.add(POSVERB);rv.add( VERBFORMGER);break;
            case TWEEDE_PERSOON_OTT_BELEEFDHEID:
                rv.add(POSVERB);rv.add(PERSON2);rv.add(NUMBERSING);rv.add(TENSEPRES);rv.add(POLITEFORM);break;
            case TWEEDE_PERSOON_OVT_BELEEFDHEID:
                rv.add(POSVERB);rv.add(PERSON2);rv.add(NUMBERSING);rv.add(TENSEPAST);rv.add(POLITEFORM);break;
            case HOOFDTELWOORD:
                rv.add(POSNUM);rv.add( NUMTYPECARD);break;
            case RANGTELWOORD:
                rv.add(POSNUM);rv.add( NUMTYPEORD);break;
            case ENKELVOUD:
                rv.add(POSNOUN);rv.add( NUMBERSING);break;
            case MEERVOUD:
                rv.add(POSNOUN);rv.add(NUMBERPLUR);break;
            case VERKLEININGSVORM:
                rv.add(POSNOUN);rv.add(DEGREEDIM);break;
            case STELLENDE_TRAP_ONVERBOGEN:
                rv.add(DEGREEPOS);rv.add(POSADJ);rv.add(PREDICATEYES);rv.add(INFLECTIONUNINF);break;
            case STELLENDE_TRAP_VERBOGEN:
                rv.add(DEGREEPOS);rv.add(POSADJ);rv.add(NUMBERSING);rv.add(POSNOUN);rv.add(INFLECTIONINFL);break;
            case VERGROTENDE_TRAP_ONVERBOGEN:
                rv.add(POSADJ);rv.add( DEGREECMP);rv.add(INFLECTIONUNINF);break;
            case VERGROTENDETRAP_VERBOGEN:
                rv.add(POSADJ);rv.add( DEGREECMP);rv.add(INFLECTIONINFL);break;
            case OVERTREFFENDE_TRAP_ONVERBOGEN:
                rv.add(POSADJ);rv.add( DEGREESUP);rv.add(INFLECTIONUNINF);break;
            case OVERTREFFENDE_TRAP_VERBOGEN:
                rv.add(POSADJ);rv.add( DEGREESUP);rv.add(INFLECTIONINFL);break;
            case ZELFSTANDIG_GEBRUIK_OP_S:
                rv.add(POSADJ);rv.add(CASEPAR);break;
            case LIDWOORDVORM:
                rv.add(POSDET); rv.add(PRONTYPEART);break;
            case BIJWOORDVORM:
                rv.add(POSADV);break;
            case SUBJECTSVORM:
                rv.add(POSPRON);rv.add(PRONTYPEPRS);rv.add(CASENOM);break;
            case OBJECTSVORM:
                rv.add(POSPRON);rv.add(PRONTYPEPRS);rv.add(CASEACC);break;
            case VOORZETSELVORM:
                rv.add(POSADP); break;
            case VOEGWOORDVORM:
                rv.add(POSCCONJ); break;
            case TUSSENWERPSELVORM:
                rv.add(POSINTJ); break;
            default:
                rv.add(nfwLinguistics + ".unknown");
        }
        return rv;
    }

    /**
     * returns hyphanation for a split form, given hyphenation for non split form
     * @param splitForm
     * @param nonSplitHyphenation
     * @return
     */
    public static String hyphenationForSplit(String splitForm, String nonSplitHyphenation) {
        String prefix = splitForm.substring(splitForm.lastIndexOf(' ') + 1);
        // the hyphenation for prefix is the start of the hyphanation for the form
        char[] chars = nonSplitHyphenation.toCharArray();
        short split = 0;
        short dot = 0;
        for (short i = 0; i < chars.length && i-dot < prefix.length(); i++) {
            if (chars[i]=='.') {
                dot++;
            } else {
                if (chars[i]==prefix.charAt(i-dot)) {
                    split = i;
                }
            }
        }
        return nonSplitHyphenation.substring(split+2) + " " + nonSplitHyphenation.substring(0,split+1);


    }

    /**
     * Assumes a String in the form of {@link Collection#toString()}, returns a
     * collection of strings. Each string contains zero or more space separated
     * universal codes, together denoting the linguistics of a word(form).
     *
     * @param linguistics
     * @return
     */
    public static Collection<String> parseLinguistics(String linguistics) {
        Set<String> rv = new HashSet<>();
        if (linguistics != null && !linguistics.isEmpty() && linguistics.startsWith("[") && linguistics.endsWith("]")) {
            Arrays.asList(linguistics.substring(1, linguistics.length() - 1).split(",")).forEach((u) -> {
                rv.add(u.trim());
            });
        }
        return rv;
    }

}
