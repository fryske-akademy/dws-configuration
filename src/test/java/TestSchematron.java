import name.dmaus.schxslt.Result;
import name.dmaus.schxslt.Schematron;
import name.dmaus.schxslt.SchematronException;
import org.fryske_akademy.validation.FA_SchematronHelper;
import org.xml.sax.InputSource;

import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSchematron {

    @Test
    public void testValidation() throws IOException, SchematronException {
        String xml = new String(Files.readAllBytes(new File("src/test/resources/achter.xml").toPath()));
        String xmlnok = new String(Files.readAllBytes(new File("src/test/resources/fout.xml").toPath()));

        Schematron schematron = new Schematron(new SAXSource(new InputSource(
                FA_SchematronHelper.class.getResourceAsStream("/schematron/tei_dictionaries.sch"))));

        Result result = schematron.validate(new SAXSource(new InputSource(new StringReader(xml))));
        if (!result.isValid()) {
            Assertions.fail("should be valid: " + result.getValidationMessages().toString());
        }

        result = schematron.validate(new SAXSource(new InputSource(new StringReader(xmlnok))));
        if (result.isValid()) {
            Assertions.fail("should not be valid");
        }
    }
}
