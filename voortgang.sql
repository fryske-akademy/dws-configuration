--echo "select * from voortgang;"|mysql -udwsuser -h 194.171.192.113 -p dws > ~/volumes/nfw-voortgang-`date +%m-%d-%Y`.txt
CREATE VIEW voortgang AS
    select redacteur,
           count(case when artikelfase=3 then '' end) as bewerking,
           count(case when artikelfase=1 then '' end) as review,
           count(case when artikelfase=2 then '' end) as eindredactie,
           count(case when artikelfase=8 then '' end) as klaar,
           count(case when artikelfase=12 then '' end) as published,
           count('') as totaal
    from artikel
    where redacteur > 61 and artikelfase in (3,1,2,8,12)
    group by redacteur with rollup;
