#!/bin/bash
source `dirname $0`/env.sh

export APPNAME=dwspublish
export VERSION=1.12
cd $BUILDROOT/dwspublish
docker build -t ${APPNAME}:${VERSION} .
docker service rm dwspublish_dwspublish
docker stack deploy --compose-file docker-compose.yml $APPNAME