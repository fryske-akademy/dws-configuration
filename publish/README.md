# docker setup for publishing dws to TEI dictionary

## environment
```bash
export APPNAME=dwspublish
export VERSION=xxx
export VOLUMEROOT=${HOME}/volumes
export DOCKER_BUILDKIT=1
```

## build image:
In the Dockerfile you find optional arguments  
During maven build a zip is assembled holding everything needed for running as docker app
```
unzip dws-publish-${VERSION}.zip
docker build -t ${APPNAME}:${VERSION} .
```
This build will have the admin password changed.

## run image

```
once: create ${VOLUMEROOT}/${APPNAME}/config/${APPNAME}.properties (with smtp, to, adminmail and from in it)
once: copy hibernate.properties to ${VOLUMEROOT}/${APPNAME}/config, adapt it
once: copy dwsToTEI.xslt to ${VOLUMEROOT}/${APPNAME}
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
docker stack deploy --compose-file docker-compose.yml ${APPNAME}
```
## crontab entry for restart (fixes mariadb/jdbc not fetching actual data)
```
sudo cp cronrestartdwspub /etc/cron.daily
sudo chmod +x /etc/cron.daily/cronrestartdwspub
cp dwspublish.sh $BUILDROOT/build-start-scripts
chmod +x $BUILDROOT/build-start-scripts/dwspublish.sh
```
### usefull commands

```
docker container|service|image|stack ls
docker service|container logs (-f) ${APPNAME}
docker stack rm ${APPNAME}
docker exec -it <container> "/bin/bash"
```
