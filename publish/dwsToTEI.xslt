<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns="http://www.tei-c.org/ns/1.0" xmlns:saxon="http://saxon.sf.net/"
                xmlns:gram="java:org.fryske_akademy.dws.grammar.FoarkarswurdlistHelper"
                xmlns:onfw="http://fryske_akademy/onfw" xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="xs gram onfw saxon tei"
                version="3.0">

    <!--
    java -cp ../farepo/org/fa/dws-publish/x.y/dws-publish-x.y.jar:../saxonpe/saxon9pe.jar net.sf.saxon.Transform -xsl:dwsToTEI.xslt -s:xxx.xml > log.txt  2>&1

    or

    java -cp ../farepo/org/fa/dws-publish/x.y/dws-publish-x.y.jar:../saxonpe/saxon9pe.jar net.sf.saxon.Transform -xsl:dwsToTEI.xslt -s:xxx.xml voorredactie=true > log.txt  2>&1
    -->


    <xsl:output indent="yes" encoding="utf-8"/>

    <xsl:param name="voorredactie" select="false()"/>
    <xsl:param name="user" select="'nouser'"/>
    <xsl:param name="status" select="'nostatus'"/>
    <xsl:param name="printheader" select="'false'"/>
    <xsl:param name="datadir" select="'file:/data/'"/>
    <xsl:param name="onfwdir" select="'file:/onfw/'"/>

    <xsl:variable name="wortel" select="/"/>


    <xsl:template match="/">
        <xsl:if test="boolean($printheader)">
            <xsl:result-document href="{$datadir}onfw_metadata.xml">
                <teiCorpus xml:lang="fry">
                    <teiHeader>
                        <fileDesc>
                            <titleStmt>
                                <title>Online Nederlands Fries Woordenboek</title>
                                <author><persName>Ylse de Boer MA</persName></author>
                                <author><persName>drs. Pieter Duijff</persName></author>
                                <author><persName>Andre Looijenga</persName></author>
                                <author><persName>Martijn Kingma</persName></author>
                                <author><persName>dr. Frits van der Kuip</persName></author>
                                <author><persName>drs. Hindrik Sijens</persName></author>
                                <author><persName>Janneke Spoelstra MA</persName></author>
                                <author><persName>dr. Willem Visser</persName></author>
                                <author><persName>Johan van der Zwaag</persName></author>
                            </titleStmt>
                            <publicationStmt>
                                <publisher>
                                    <orgName>Fryske Akademy</orgName>
                                </publisher>
                                <availability status="free">
                                    <licence target="http://creativecommons.org/licenses/by-sa/4.0/">
                                        <graphic url="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
                                        This work is licensed under a
                                        <ref target="http://creativecommons.org/licenses/by-sa/4.0/">Creative
                                            Commons Attribution-ShareAlike 4.0 International License.
                                        </ref>
                                    </licence>
                                </availability>
                            </publicationStmt>
                            <sourceDesc>
                                <msDesc>
                                    <msIdentifier>
                                        <country>Nederland</country>
                                        <region>Friesland</region>
                                        <settlement>Leeuwarden</settlement>
                                        <institution>Fryske Akademy</institution>
                                        <repository>onfw</repository>
                                        <idno>ONFW</idno>
                                        <msName>Online Nederlands Fries Woordenboek</msName>
                                    </msIdentifier>
                                    <p xml:lang="en">Dutch words and their and Frisian translation,
                                        with paradigm, grammar, examples, collocations and proverbs.
                                    </p>
                                    <p xml:lang="nl">Nederlandse woorden met hun Friese vertaling,
                                        paradigma, grammatica, voorbeelden, vaste verbindingen en spreekwoorden.
                                    </p>
                                    <p xml:lang="fry">Nederlânske wurden mei Fryske oersetting,
                                        paradigma, grammatika, foarbylden, fêste ferbiningen en sprekwurden.
                                    </p>
                                </msDesc>
                            </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                            <langUsage>
                                <language ident="fry">Frysk</language>
                                <language ident="nl">Nederlands</language>
                            </langUsage>
                        <textDesc>
                            <purpose type="formtranslation"/>
                            <purpose type="textsearch"/>
                            <purpose type="hyphenation"/>
			                <purpose type="grammar"/>
                            <purpose type="paradigm"/>
                            <purpose type="examples"/>
                            <purpose type="collocations"/>
                            <purpose type="proverbs"/>
                            <purpose type="usage"/>
                        </textDesc>				    
                        </profileDesc>
                        <encodingDesc>
                            <tagsDecl>
                                <namespace name="http://www.tei-c.org/ns/1.0">
                                    <tagUsage gi="gram">
                                        <ref target="https://bitbucket.org/fryske-akademy/tei-encoding/">
                                            Grammar encoding follows extended universaldependencies
                                        </ref>
                                    </tagUsage>
                                </namespace>
                            </tagsDecl>
                        </encodingDesc>
                        <revisionDesc>
                            <list>
                                <item>converted on
                                    <date when="{current-dateTime()}"/>
                                </item>
                            </list>
                        </revisionDesc>
                    </teiHeader>
                </teiCorpus>
            </xsl:result-document>
        </xsl:if>
        <xsl:variable name="art" select="/artikel/Lemma/Lemmavorm/text()"/>
        <xsl:choose>
            <xsl:when test="$status='13' or $status='3' or $status='0'">
<!--                <xsl:if test="//Oersetting">-->
<!--                    <xsl:message><xsl:value-of select="$art"/>: foutieve status "net opnommen"?</xsl:message>-->
<!--                </xsl:if>-->
                <ok/>
            </xsl:when>
            <xsl:when test="$art">
                <TEI xml:lang="nl" linguisticsversion="2">
                    <teiHeader>
                        <fileDesc>
                            <titleStmt>
                                <title>
                                    <xsl:value-of select="$art"/>
                                </title>
                            </titleStmt>
                            <publicationStmt>
                                <publisher>
                                    <orgName>Fryske Akademy</orgName>
                                </publisher>
                                <availability status="free">
                                    <tei:licence target="http://creativecommons.org/licenses/by-sa/4.0/">
                                        <tei:graphic url="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
                                        This work is licensed under a
                                        <tei:ref  target="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License.</tei:ref>
                                    </tei:licence>
                                </availability>
                            </publicationStmt>
                            <sourceDesc>
                                <xsl:if test="$voorredactie">
                                    <listPerson>
                                        <person role="author">
                                            <persName>
                                                <xsl:value-of select="$user"/>
                                            </persName>
                                        </person>
                                    </listPerson>
                                </xsl:if>
                                <xsl:variable name="stat">
                                    <xsl:choose>
                                        <xsl:when test="$status='1'">candidate</xsl:when>
                                        <xsl:when test="$status='2'">approved</xsl:when>
                                        <xsl:when test="$status='8'">submitted</xsl:when>
                                        <xsl:when test="$status='12'">published</xsl:when>
                                        <xsl:otherwise>status_<xsl:value-of select="$status"/></xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <msDesc status="{$stat}">
                                    <msIdentifier>
                                        <collection ref="file:onfw_metadata.xml" key="ONFW"/>
                                        <idno>
                                            <xsl:value-of select="$art"/>
                                        </idno>
                                    </msIdentifier>
                                </msDesc>
                            </sourceDesc>
                        </fileDesc>
                        <revisionDesc>
                            <list>
                                <item>converted on
                                    <date when="{current-dateTime()}"/>
                                </item>
                            </list>
                        </revisionDesc>
                    </teiHeader>
                    <text>
                        <body>
                            <xsl:apply-templates select="/artikel"/>
                        </body>
                    </text>
                </TEI>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="$art"/>: ontbrekende /artikel/Lemma/Lemmavorm</xsl:message>
                <ok/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="artikel[Koepelartikel]">
        <superEntry>
            <xsl:apply-templates select="Koepelartikel"/>
            <xsl:if test="spreekwoordenBody">
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: spreekwoordenBody foutief op hoofdniveau bij koepelartikel</xsl:message>
            </xsl:if>
        </superEntry>
    </xsl:template>

    <xsl:template match="artikel[not(Koepelartikel)]">
        <entry>
            <xsl:call-template name="entry"/>
        </entry>
    </xsl:template>

    <xsl:template match="artikel" mode="copy">
        <!-- not copying, only link -->
        <xsl:param name="lemma"/>
        <xsl:param name="pid"/>
        <xsl:if test="$pid">
            <xsl:variable name="link" select="concat($lemma, '.xml')"/>
            <ref target="{$link}">
                <xsl:value-of select="$link"/>
            </ref>
        </xsl:if>
    </xsl:template>

    <xsl:template match="Koepelartikel|Kernbetekenis|Subbetekenis" mode="copy">
        <!-- not copying, only link -->
        <xsl:param name="lemma"/>
        <xsl:param name="pid"/>
        <xsl:if test="$pid">
            <xsl:variable name="link" select="concat(if ($lemma) then concat($lemma, '.xml')  else '','#pid_', $pid)"/>
            <ref target="{$link}">
                <xsl:value-of select="$link"/>
            </ref>
        </xsl:if>
    </xsl:template>

    <xsl:template match="Koepelartikel">
        <entry>
            <xsl:if test="@pid">
                <xsl:attribute name="xml:id" select="concat('pid_', @pid)"/>
            </xsl:if>
            <xsl:call-template name="entry"/>
        </entry>
    </xsl:template>

    <xsl:template name="entry">
        <xsl:if test="parent::Koepelartikel">
            <xsl:if test="not(Lemma)">
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: ontbrekend Koepelartikel/Lemma</xsl:message>
            </xsl:if>
            <xsl:if test="not(KoepelBetekenisaanduiding) or not(KoepelBetekenisaanduiding/text()) or KoepelBetekenisaanduiding/text() = ''">
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: ontbrekende Koepelartikel/KoepelBetekenisaanduiding</xsl:message>
            </xsl:if>
        </xsl:if>
        <xsl:if test="not(Woordsoort)">
            <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: ontbrekende Woordsoort</xsl:message>
        </xsl:if>
        <form type="lemma" xml:lang="nl">
            <orth>
                <xsl:value-of select="if (parent::Koepelartikel) then Lemma/Lemmavorm else /artikel/Lemma/Lemmavorm"/>
            </orth>
            <xsl:apply-templates select="Woordsoort"/>
            <xsl:apply-templates select="../Uitspraak/FonetischeSchrijfwijze"/>
        </form>
        <xsl:apply-templates select="KoepelBetekenisaanduiding"/>
        <xsl:apply-templates select="BetekenisEnGebruik"/>
        <xsl:apply-templates select="spreekwoordenBody/Spreekwoorden"/>
    </xsl:template>

    <xsl:template match="KoepelBetekenisaanduiding">
        <note>
            <xsl:value-of select="."/>
        </note>
    </xsl:template>

    <xsl:template match="BetekenisEnGebruik">
        <xsl:apply-templates select="Kernbetekenis/betekenisInfo"/>
    </xsl:template>

    <xsl:template match="betekenisInfo">
        <xsl:if test="combinatiesBody">
            <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: heeft combinatiesBody</xsl:message>
        </xsl:if>
        <xsl:element name="sense" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:if test="../@pid">
                <xsl:attribute name="xml:id" select="concat('pid_', ../@pid)"/>
            </xsl:if>
            <xsl:apply-templates select="definitieBody"/>
            <xsl:if test="not(Oersettingen/Oersetting[Foarm!='---'])">
                <xsl:call-template name="checkVertaling">
                    <xsl:with-param name="oersettingen" select="Oersettingen"/>
                </xsl:call-template>
            </xsl:if>
            <xsl:apply-templates select="Oersettingen"/>
            <xsl:apply-templates select="voorbeeldBody/Voorbeeld"/>
            <xsl:apply-templates select="verbindingenBody/Verbindingen"/>
            <xsl:apply-templates select="spreekwoordenBody/Spreekwoorden"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="checkVertaling">
        <xsl:param name="oersettingen"/>
        <xsl:param name="msg" select="''"/>
        <xsl:variable name="txt">ie de voorbeelden voor</xsl:variable>
        <xsl:variable name="txt2">alleen in vaste verbindingen</xsl:variable>
        <xsl:choose>
            <xsl:when
                    test="$oersettingen/Oersetting[contains(OersetTaljochting,$txt) or contains(OersetTaljochting,$txt2)]"/>
            <xsl:when test="$oersettingen[contains(Taljochting,$txt) or contains(Taljochting,$txt2)]"/>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: ontbrekende vertaling <xsl:value-of
                        select="$msg"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Verbindingen[not(ArtikelRef)]|Spreekwoorden[not(ArtikelRef)]">
        <xsl:apply-templates select="." mode="copy"/>
    </xsl:template>

    <xsl:template match="Verbindingen[not(ArtikelRef)]|Spreekwoorden[not(ArtikelRef)]" mode="copy">
        <xsl:param name="lemma"/>
        <xsl:param name="pid"/>
        <xsl:param name="genpid" select="generate-id()"/>
        <cit type="{if (name()='Verbindingen') then 'collocation' else 'proverb'}" xml:id="{if (@pid and not($pid)) then concat('pid_', @pid) else $genpid}">
            <quote>
                <xsl:call-template name="markup">
                    <xsl:with-param name="txt" select="Vorm/text()"/>
                </xsl:call-template>
                <xsl:apply-templates select="definitieBody/Definitieaanvulling/SemantischeCollocator"/>
                <xsl:if test="$pid">
                    <xsl:comment>copied for
                        <xsl:value-of select="$lemma/ancestor::artikel/Lemma/Lemmavorm"/> from
                        <xsl:value-of select="$lemma"/>
                    </xsl:comment>
                    <xsl:variable name="link"
                                  select="concat(if ($lemma) then concat($lemma, '.xml')  else '','#pid_', $pid)"/>
                    <note><ref target="{$link}">copy of
                        <xsl:value-of select="if ($lemma) then $lemma else $pid"/>
                    </ref></note>
                </xsl:if>
            </quote>
            <xsl:choose>
                <xsl:when test="count(Info) > 1">
                    <cit type="sensegroup">
                        <xsl:apply-templates select="Info"/>
                    </cit>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="Info"/>
                </xsl:otherwise>
            </xsl:choose>
        </cit>
    </xsl:template>

    <xsl:template match="Spreekwoorden[ArtikelRef]|Verbindingen[ArtikelRef]">
        <xsl:variable name="type" select="ArtikelRef/link/dtype"/>
        <xsl:choose>
            <xsl:when test="$type='ver' or $type='spr'">
                <xsl:apply-templates select="ArtikelRef/link"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="cit" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="type" select="if (name()='Verbindingen') then 'collocation' else 'proverb'"/>
                    <xsl:variable name="id" select="generate-id(.)"/>
                    <xsl:attribute name="xml:id" select="$id"/>
                    <quote>
                        <xsl:call-template name="markup">
                            <xsl:with-param name="txt" select="Vorm/text()"/>
                        </xsl:call-template>
                    </quote>
                    <xsl:if test="ArtikelRef/link">
                        <note><xsl:apply-templates select="ArtikelRef/link"/></note>
                    </xsl:if>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="link">
        <xsl:param name="genpid" select="generate-id()"/>
        <!--
eduard@eduard-X756UB:~/exist-db/onfw$ find . -exec grep -o '<.* pid="' {} \;|grep -v '<artikel'|sort|uniq
<Kernbetekenis pid="
<Koepelartikel pid="
<Spreekwoorden pid="
<Subbetekenis pid="
<Verbindingen pid="
<Voorbeeld pid="

eduard@eduard-X756UB:~/exist-db/onfw$ find . -exec grep -o '<dtype[^/]*/' {} \;|sort|uniq
<dtype>art</
<dtype>bet</
<dtype>spr</
<dtype>ver</

De strategie nu is dupliceren info uit een link, willen we dat zo houden of gaan we linken?

Voorlopig kopieren omdat we nog geen redactie doen. En ook een ptr opnemen.

teveel recursie:

Als 
        -->
        <xsl:variable name="pid" select="pid"/>
        <xsl:variable name="parentpid" select="ancestor::*[@pid]/@pid[1]"/>
        <xsl:choose>
            <xsl:when test="preceding::*[@pid=$pid]//pid=$parentpid">
                <!-- wsl. recursie-->
                <xsl:variable name="target" select="//*[@pid=$pid]"/>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: cyclic reference assumed to <xsl:value-of
                    select="lemma"/>#<xsl:value-of select="$pid"/><xsl:value-of
                        select="concat(' for ',$target//link/desc)"/></xsl:message>
            </xsl:when>
            <xsl:when test="lemma=/artikel/Lemma/Lemmavorm and //*[@pid=$pid]">
                <xsl:variable name="target" select="//*[@pid=$pid]"/>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: copying for <xsl:value-of
                        select="name(../..)"/> from <xsl:value-of
                        select="concat('#',$pid)"/></xsl:message>
                <xsl:call-template name="checkLink">
                    <xsl:with-param name="possibleLink" select="$target"/>
                    <xsl:with-param name="pid" select="$pid"/>
                    <xsl:with-param name="genpid" select="$genpid"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when
                    test="doc-available(concat($onfwdir,lemma,'.xml')) and doc(concat($onfwdir,lemma,'.xml'))//*[@pid=$pid]">
                <xsl:variable name="target" select="doc(concat($onfwdir,lemma,'.xml'))//*[@pid=$pid]"/>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: copying for <xsl:value-of
                        select="name(../..)"/> from <xsl:value-of select="concat(lemma,'.xml#',$pid)"/></xsl:message>
                <xsl:call-template name="checkLink">
                    <xsl:with-param name="possibleLink" select="$target"/>
                    <xsl:with-param name="pid" select="$pid"/>
                    <xsl:with-param name="genpid" select="$genpid"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: <xsl:value-of
                        select="concat($onfwdir,lemma,'.xml',' pid:', $pid)"/> missing or invalid, referred from <xsl:value-of
                        select="name(../..)"/></xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="checkLink">
        <xsl:param name="possibleLink"/>
        <xsl:param name="pid"/>
        <xsl:param name="genpid"/>
        <xsl:choose>
            <xsl:when test="$possibleLink/ArtikelRef/link">
                <xsl:choose>
                    <!-- check one level of recursion for now-->
                    <xsl:when test="$possibleLink/ArtikelRef/link/pid=$pid/../../../@pid">
                        <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: cyclic reference to <xsl:value-of
                            select="lemma"/>#<xsl:value-of select="$pid"/><xsl:value-of
                                select="concat(' for ',$possibleLink/ArtikelRef/link/desc)"/></xsl:message>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="$possibleLink/ArtikelRef/link">
                            <xsl:with-param name="genpid" select="$genpid"/>
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="$possibleLink" mode="copy">
                    <xsl:with-param name="lemma" select="lemma"/>
                    <xsl:with-param name="pid" select="$pid"/>
                    <xsl:with-param name="genpid" select="$genpid"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Info">
        <xsl:if test="not(definitieBody)">
            <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: ontbrekende definitie</xsl:message>
        </xsl:if>
        <xsl:variable name="info">
            <xsl:apply-templates select="definitieBody"/>
            <xsl:if test="not(Oersettingen/Oersetting[Foarm!='---'])">
                <xsl:call-template name="checkVertaling">
                    <xsl:with-param name="oersettingen" select="Oersettingen"/>
                    <xsl:with-param name="msg" select="concat(' van: ',../Vorm)"/>
                </xsl:call-template>
            </xsl:if>
            <xsl:apply-templates select="Oersettingen"/>
            <xsl:apply-templates select="voorbeeldBody/Voorbeeld"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="following-sibling::Info or preceding-sibling::Info">
                <sense><xsl:copy-of select="$info"/></sense>
            </xsl:when>
            <xsl:otherwise><xsl:copy-of select="$info"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="Voorbeeld">
        <xsl:for-each select="TekstPlusOersettingen">
            <cit type="example">
                <quote>
                    <xsl:call-template name="markup">
                        <xsl:with-param name="txt" select="Tekst/text()"/>
                    </xsl:call-template>
                    <bibl>
                        <xsl:if test="../Auteur">
                            <author>
                                <persName>
                                    <xsl:value-of select="../Auteur"/>
                                </persName>
                            </author>
                        </xsl:if>
                        <xsl:choose>
                            <xsl:when test="../Bron">
                                <msIdentifier>
                                    <xsl:if test="../BronID">
                                        <idno>
                                            <xsl:value-of select="../BronID"/>
                                        </idno>
                                    </xsl:if>
                                    <msName>
                                        <xsl:value-of select="../Bron"/>
                                    </msName>
                                </msIdentifier>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="../BronID and not(../URL)">
                                    <msIdentifier>
                                        <idno>
                                            <xsl:value-of select="../BronID"/>
                                        </idno>
                                    </msIdentifier>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="../URL">
                            <xsl:variable name="url">
                                <xsl:try>
                                    <xsl:value-of select="xs:anyURI(../URL)"/>
                                    <xsl:catch>
                                        <xsl:value-of select="''"/>
                                    </xsl:catch>
                                </xsl:try>
                            </xsl:variable>
                            <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:if test="$url!='' and not(contains($url,' '))">
                                    <xsl:attribute select="$url" name="target"/>
                                </xsl:if>
                                <xsl:if test="../BronID">
                                    <idno>
                                        <xsl:value-of select="../BronID"/>
                                    </idno>
                                </xsl:if>
                                <xsl:value-of select="../URL"/>
                            </xsl:element>
                        </xsl:if>
                        <xsl:if test="../Bladzijde">
                            <biblScope unit="page">
                                <xsl:value-of select="../Bladzijde"/>
                            </biblScope>
                        </xsl:if>
                        <xsl:if test="../Datering">
                            <date>
                                <xsl:value-of select="../Datering"/>
                            </date>
                        </xsl:if>
                    </bibl>
                </quote>
                <xsl:if test="not(Oersettingen/Oersetting[Foarm!='---'])">
                    <xsl:call-template name="checkVertaling">
                        <xsl:with-param name="oersettingen" select="Oersettingen"/>
                        <xsl:with-param name="msg" select="concat(' van: ',Tekst)"/>
                    </xsl:call-template>
                </xsl:if>
                <xsl:apply-templates select="Oersettingen"/>
            </cit>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="definitieBody">
        <xsl:if test="Definitie or Minidefinitie or Definitieaanvulling/Opmerking or Definitieaanvulling/Verwijzing">
            <def>
                <!--            Definitie Minidefinitie Definitieaanvulling-->
                <xsl:apply-templates select="Definitieaanvulling/SemantischeCollocator"/>
                <xsl:apply-templates select="Minidefinitie"/>
                <xsl:apply-templates select="Definitie"/>
                <xsl:choose>
                    <xsl:when test="Definitieaanvulling/Opmerking">
                        <xsl:apply-templates select="Definitieaanvulling/Opmerking"/>
                    </xsl:when>
                    <xsl:when test="Definitieaanvulling/Verwijzing">
                        <xsl:variable name="cp">
                            <xsl:apply-templates select="//*[@pid=Definitieaanvulling/Verwijzing/link/pid]/Vorm"/>
                        </xsl:variable>
                        <xsl:if test="$cp">
                            <note>
                                <xsl:value-of select="$cp"/>
                                <xsl:variable name="link" select="concat('#pid_', Definitieaanvulling/Verwijzing/link/pid)"/>
                                <ref target="{$link}">
                                    <xsl:value-of select="$link"/>
                                </ref>
                            </note>
                        </xsl:if>
                    </xsl:when>
                </xsl:choose>
            </def>
        </xsl:if>
    </xsl:template>

    <xsl:template match="Opmerking">
        <note>
            <xsl:value-of select="text()"/>
            <xsl:apply-templates select="../Verwijzing"/>
        </note>
    </xsl:template>

    <xsl:template match="Verwijzing">
        <xsl:apply-templates select="link"/>
    </xsl:template>

    <xsl:template match="SemantischeCollocator">
        <etym>
            <usg type="concerning">
                <xsl:value-of select="replace(lower-case(Aanloopzin),'...','')"/><xsl:value-of select="Restrictie"/>
            </usg>
        </etym>
    </xsl:template>

    <xsl:template match="Minidefinitie[../Definitie]">
        <label>
            <xsl:value-of select="."/>
        </label>
    </xsl:template>

    <xsl:template match="Definitie | Minidefinitie[not(../Definitie)]">
        <gloss>
            <xsl:value-of select="."/>
        </gloss>
    </xsl:template>

    <xsl:template match="Oersettingen">
        <cit type="translation" xml:lang="fry">
            <xsl:apply-templates select="Oersetting"/>
            <xsl:apply-templates select="Taljochting"/>
        </cit>
    </xsl:template>

    <xsl:template match="Oersettingen/Taljochting">
        <note>
            <xsl:value-of select="."/>
        </note>
    </xsl:template>

    <xsl:template match="Oersetting">
        <xsl:variable name="etym">
            <xsl:if test="Gebrûk/*">
                <etym>
                    <xsl:if test="Gebrûk/Oorsprong">
                        <xsl:attribute name="type">borrowing</xsl:attribute>
                    </xsl:if>
                    <xsl:for-each select="Gebrûk/*">
                        <usg type="{onfw:usg(name())}">
                            <xsl:value-of select="normalize-space(.)"/>
                        </usg>
                        <xsl:if test=".='Hollandisme'">
                            <xsl:message>Hollandisme:<xsl:value-of select="../../Foarm"/></xsl:message>
                        </xsl:if>
                    </xsl:for-each>
                </etym>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="note">
            <xsl:if test="OersetTaljochting">
                <note>
                    <xsl:value-of select="OersetTaljochting"/>
                </note>
            </xsl:if>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="name(../..)='betekenisInfo'">
                <form type="lemma" xml:lang="fry">
                    <orth>
                        <xsl:value-of select="Foarm"/>
                    </orth>
                    <xsl:choose>
                        <xsl:when test="Woordsoort">
                            <xsl:apply-templates select="Woordsoort"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="checkVertaling">
                                <xsl:with-param name="oersettingen" select=".."/>
                                <xsl:with-param name="msg" select="concat(' van: ',Foarm)"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:copy-of select="$etym"/>
                    <xsl:copy-of select="$note"/>
                </form>
            </xsl:when>
            <xsl:otherwise>
                <quote>
                    <xsl:call-template name="markup">
                        <xsl:with-param name="txt" select="Foarm/text()"/>
                    </xsl:call-template>
                    <xsl:copy-of select="$etym"/>
                    <xsl:copy-of select="$note"/>
                </quote>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="OersetFoarbyld">
            <note>
                <xsl:value-of select="OersetFoarbyld"/>
            </note>
        </xsl:if>
    </xsl:template>

    <xsl:template match="Uitspraak/FonetischeSchrijfwijze">
        <pron notation="IPA">
            <xsl:value-of select="."/>
        </pron>
    </xsl:template>

    <xsl:template name="pos">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="type" select="$woordsoort/Type"/>
        <xsl:choose>
            <xsl:when test="$type='substantief' and $woordsoort/Substantief/Naamtype='eigennaam'">propn</xsl:when>
            <xsl:when test="$type='substantief'">noun</xsl:when>
            <xsl:when test="$type='werkwoord'">
                <xsl:choose>
                    <xsl:when test="$woordsoort/Werkwoord/Functie != 'zelfstandig werkwoord'">aux</xsl:when>
                    <xsl:otherwise>verb</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$type='adjectief'">adj</xsl:when>
            <xsl:when test="$type='lidwoord'">det</xsl:when>
            <xsl:when test="$type='voornaamwoord'">pron</xsl:when>
            <xsl:when test="$type='telwoord'">num</xsl:when>
            <xsl:when test="$type='bijwoord'">adv</xsl:when>
            <xsl:when test="$type='voorzetsel'">adp</xsl:when>
            <xsl:when test="$type='voegwoord'">
                <xsl:choose>
                    <xsl:when test="Voegwoord/Soort='onderschikkend'">sconj</xsl:when>
                    <xsl:otherwise>cconj</xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="$type='tussenwerpsel'">intj</xsl:when>
            <xsl:when test="$type='overige'">
                <xsl:value-of select="'x'"/>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Geen pos for <xsl:value-of
                        select="$type"/></xsl:message>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="ww">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="soort" select="$woordsoort/Werkwoord/SyntactischeSubklasse"/>
        <xsl:if test="contains($soort,'wederkerend')">
            <gram>reflex.yes</gram>
        </xsl:if>
    </xsl:template>

    <xsl:template name="vnw">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="soort" select="$woordsoort/Voornaamwoord/Soort"/>
        <xsl:choose>
            <xsl:when test="$soort='persoonlijk voornaamwoord'">
                <gram>prontype.prs</gram>
            </xsl:when>
            <xsl:when test="$soort='wederkerend voornaamwoord'">
                <gram>prontype.prs</gram>
            </xsl:when>
            <xsl:when test="$soort='wederkerig voornaamwoord'">
                <gram>prontype.rcp</gram>
            </xsl:when>
            <xsl:when test="$soort='bezittelijk voornaamwoord'">
                <gram>prontype.prs</gram>
            </xsl:when>
            <xsl:when test="$soort='aanwijzend voornaamwoord'">
                <gram>prontype.dem</gram>
            </xsl:when>
            <xsl:when test="$soort='vragend voornaamwoord'">
                <gram>prontype.int</gram>
            </xsl:when>
            <xsl:when test="$soort='betrekkelijk voornaamwoord'">
                <gram>prontype.rel</gram>
            </xsl:when>
            <xsl:when test="$soort='onbepaald voornaamwoord'">
                <gram>prontype.ind</gram>
            </xsl:when>
            <xsl:when test="$soort='uitroepend voornaamwoord'">
                <gram>prontype.exc</gram>
            </xsl:when>
        </xsl:choose>
        <xsl:if test="contains($soort,'wederkerend')">
            <gram>reflex.yes</gram>
        </xsl:if>
        <xsl:if test="contains($soort,'bezittelijk')">
            <gram>poss.yes</gram>
        </xsl:if>
    </xsl:template>

    <xsl:template name="adj">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="subklasse" select="$woordsoort/Adjectief/SyntactischeSubklasse"/>
        <xsl:choose>
            <xsl:when test="contains($subklasse,'attributief')">
                <gram>construction.attr</gram>
            </xsl:when>
            <xsl:when test="contains($subklasse,'predicatief')">
                <gram>predicate.yes</gram>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="noun">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="getal" select="$woordsoort/Substantief/Getal"/>
        <xsl:choose>
            <xsl:when test="$getal='geen meervoud'">
                <gram>number.coll</gram>
            </xsl:when>
            <xsl:when test="$getal='geen enkelvoud'">
                <gram>number.ptan</gram>
            </xsl:when>
            <xsl:when test="$getal='met meervoud'"></xsl:when>
            <xsl:when test="$getal='enkelvoud, soms ook met meervoud'"></xsl:when>
            <xsl:when test="$getal='met enkelvoud'"></xsl:when>
            <xsl:when test="$getal='meervoud, soms ook met enkelvoud'"></xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="num">
        <xsl:param name="woordsoort"/>
        <xsl:variable name="soort" select="$woordsoort/Telwoord/Soort"/>
        <xsl:variable name="htw"
                      select="boolean($woordsoort/following::SpellingEnFlexie[1]/TelwoordVormen/Hoofdtelwoord)"/>
        <xsl:variable name="rtw"
                      select="boolean($woordsoort/following::SpellingEnFlexie[1]/TelwoordVormen/Rangtelwoord)"/>
        <xsl:variable name="brtw"
                      select="boolean($woordsoort/following::SpellingEnFlexie[1]/TelwoordVormen/Breukgetal)"/>
        <xsl:choose>
            <xsl:when test="$soort='bepaald hoofdtelwoord' or $htw">
                <gram>numtype.card</gram>
            </xsl:when>
            <xsl:when test="$soort='onbepaald hoofdtelwoord' or $htw">
                <gram>numtype.card</gram>
            </xsl:when>
            <xsl:when test="$soort='rangtelwoord' or $rtw">
                <gram>numtype.ord</gram>
            </xsl:when>
            <xsl:when test="$soort='breukgetal' or $brtw">
                <gram>numtype.frac</gram>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: No numtype found for telwoord</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$soort='onbepaald hoofdtelwoord'">
            <gram>prontype.ind</gram>
        </xsl:if>
    </xsl:template>

    <xsl:function name="onfw:usg">
        <xsl:param name="usg"/>
        <xsl:choose>
            <xsl:when test="$usg='Taalfariëteit'">lang</xsl:when>
            <xsl:when test="$usg='Oorsprong'">lang</xsl:when>
            <xsl:when test="$usg='Styl'">style</xsl:when>
            <xsl:when test="$usg='Hâlding'">connotation</xsl:when>
            <xsl:when test="$usg='Frekwinsje'">freq</xsl:when>
            <xsl:when test="$usg='Tiid'">time</xsl:when>
            <xsl:when test="$usg='Medium'">medium</xsl:when>
            <xsl:when test="$usg='Noat'">hint</xsl:when>
        </xsl:choose>
    </xsl:function>

    <xsl:template match="Woordsoort">
        <xsl:variable name="pos">
            <xsl:call-template name="pos">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:variable>
        <gram>
            <xsl:value-of select="concat('pos.',$pos)"/>
        </gram>
        <xsl:if test="$pos='pron'">
            <xsl:call-template name="vnw">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$pos='verb'">
            <xsl:call-template name="ww">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$pos='adj'">
            <xsl:call-template name="adj">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$pos='num'">
            <xsl:call-template name="num">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$pos='noun'">
            <xsl:call-template name="noun">
                <xsl:with-param name="woordsoort" select="."/>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test=".//Persoon">
            <gram>
                <xsl:choose>
                    <xsl:when test="contains(.//Persoon[1],'1ste')">person.1</xsl:when>
                    <xsl:when test="contains(.//Persoon[1],'2de')">person.2</xsl:when>
                    <xsl:when test="contains(.//Persoon[1],'3de')">person.3</xsl:when>
                    <xsl:otherwise>no person</xsl:otherwise>
                </xsl:choose>
            </gram>
        </xsl:if>
        <xsl:if test=".//AardLidwoord">
            <xsl:variable name="art" select="string-join(.//AardLidwoord/text(),'')"/>
            <article>
                <xsl:value-of select="if (ancestor::Oersetting) then replace($art,'het','it') else $art"/>
            </article>
        </xsl:if>
        <xsl:if test="name(..)='Oersetting' and not(following-sibling::SpellingEnFlexie)">
            <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Geen paradigma bij <xsl:value-of select="$pos"/></xsl:message>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="Type='lidwoord' and not(following-sibling::SpellingEnFlexie//LidwoordVorm)">
                <gram>prontype.art</gram>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="following-sibling::SpellingEnFlexie">
                        <xsl:apply-templates select="following-sibling::SpellingEnFlexie//*[Woordvorm]"/>
                        <xsl:apply-templates select="following-sibling::SpellingEnFlexie/*/*[not(.//Woordvorm)]"/>
                    </xsl:when>
                    <xsl:when test="/artikel/Woordsoort[Type=current()/Type]">
                        <xsl:apply-templates
                                select="/artikel/Woordsoort[Type=current()/Type]/following-sibling::SpellingEnFlexie//*[Woordvorm]"/>
                        <xsl:apply-templates
                                select="/artikel/Woordsoort[Type=current()/Type]/following-sibling::SpellingEnFlexie/*/*[not(.//Woordvorm)]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Geen paradigma bij <xsl:value-of
                                select="$pos"/></xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*[ancestor::SpellingEnFlexie][not(Woordvorm)]">
        <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Overslaan <xsl:value-of
                select="name()"/> in SpellingEnFlexie, niet ondersteund</xsl:message>
    </xsl:template>

    <xsl:template match="*[Woordvorm]">
        <xsl:variable name="pos">
            <xsl:call-template name="pos">
                <xsl:with-param name="woordsoort" select="ancestor::Oersetting/Woordsoort"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when
                    test="($pos='verb' or $pos='aux') and Woordvorm=ancestor::Oersetting/Foarm and name()!='Infinitief'">
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Overslaan <xsl:value-of
                        select="name()"/>, gelijk aan infinitief</xsl:message>
            </xsl:when>
            <xsl:when test="contains(name(),'Variant')">
                <xsl:message><xsl:value-of select="/artikel/Lemma/Lemmavorm"/>: Overslaan <xsl:value-of
                        select="name()"/>, <xsl:value-of select="Woordvorm"/></xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <form type="paradigm">
                    <orth>
                        <xsl:value-of select="Woordvorm"/>
                    </orth>
                    <xsl:call-template name="grammar">
                        <xsl:with-param name="vorm" select="name()"/>
                    </xsl:call-template>
                    <hyph>
                        <xsl:value-of select="Afbreking"/>
                    </hyph>
                </form>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="grammar">
        <xsl:param name="vorm"/>
        <xsl:variable name="universal" select="gram:toUniversal($vorm)"/>
        <xsl:for-each select="$universal">
            <xsl:if test="replace(.,'(.*)[.][^.]+$','$1')!='pos'">
                <xsl:choose>
                    <xsl:when test="ends-with(.,'.unknown')">
                        <xsl:message><xsl:value-of select="$wortel/artikel/Lemma/Lemmavorm"/>: Onbekende grammar <xsl:value-of
                                select="$vorm"/></xsl:message>
                    </xsl:when>
                    <xsl:otherwise>
                        <gram>
                            <xsl:value-of select="replace(.,'(.*)[.][^.]+$','$1')"/>
                            <xsl:value-of select="'.'"/>
                            <xsl:value-of select="replace(.,'.*[.]([^.]+)$','$1')"/>
                        </gram>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="markup">
        <xsl:param name="txt"/>
        <xsl:choose>
            <xsl:when test="contains($txt,'[i]')">
                <xsl:message><xsl:value-of select="$wortel/artikel/Lemma/Lemmavorm"/>: replacing markup</xsl:message>
                <xsl:value-of
                        disable-output-escaping="true"
                        select="replace(replace($txt,'\[i\]','&lt;q>'),'\[/i\]','&lt;/q>')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$txt"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
