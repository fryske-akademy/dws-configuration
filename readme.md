# Software to publish dictionary data edited in dws to TEI/UD based dictionary

## saxonica pe in local maven repo (once)
```
mvn install:install-file \
   -Dfile=saxonpe/saxon9pe.jar \
   -DgroupId=net.sf.saxon \
   -DartifactId=Saxon-PE \
   -Dversion=9 \
   -Dpackaging=jar \
   -DgeneratePom=true \
   -DlocalRepositoryPath=farepo
```
## how publishing works
- editors manage a mariadb database, holding custom xml
- a java app extracts the xml and converts it to TEI
- an exist-db plugin imports the TEI xml into exist-db
- an exist-db app provides access to the xml
## build application

- [see docker](publish/README.md)