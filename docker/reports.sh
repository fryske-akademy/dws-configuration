#!/bin/sh
# schrijf voortgang dagelijks naar een bestand in een docker volume en maakt een backup voor elke weekdag
echo "select * from voortgang;"|mariadb -udwsuser -p`cat /run/secrets/dwspw` dws > /reports/voortgang-`date +%m-%d-%Y`.txt
echo "select * from complex;"|mariadb -udwsuser -p`cat /run/secrets/dwspw` dws > /reports/complex.txt
echo "select * from lemma_5000;"|mariadb -udwsuser -p`cat /run/secrets/dwspw` dws > /reports/lemma_5000.txt

mariadb-dump -udwsuser -p`cat /run/secrets/dwspw` dws > /reports/backup-`date +%w`.sql