# mariadb docker setup for dws

## environment
```bash
export APPNAME=dws
export VOLUMEROOT=${HOME}/volumes
```
# prepare volumes (once)
```
mkdir -p ${VOLUMEROOT}/${APPNAME}/data
mkdir -p ${VOLUMEROOT}/${APPNAME}/config
cp config-file.cnf ${VOLUMEROOT}/${APPNAME}/config
cp reports.sh ${VOLUMEROOT}/${APPNAME}/reports
chmod +x ${VOLUMEROOT}/${APPNAME}/reports/reports.sh
```
## prepare secret (once)
**NOTE**: create the secret in /bin/sh, without history  
```
sh
docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
echo "xxxxxxxxx"|docker secret create dwspw -
```
## run
```
docker stack deploy -c docker-compose.yml ${APPNAME}
```
## crontab entry voor backups
```
cp cronreports${APPNAME} /etc/cron.daily
chmod +x /etc/cron.daily/cronreports${APPNAME}
```
## initialize database (once)
- dump old database using mysqldump
- copy dump to container using ```docker cp```
- import data via ```mysql```
